- 1.4.1 (28 Aug. 2024): Marked as compatible with 1.21.1
- 1.4.0 (16 Jun. 2024):
  - Updated for 1.21
  - Updated translations
  - Removed an incorrect error log
  - Fixed a potential load-order dependent crash when using REI
  - Minor internal changes
- 1.3.14 (11 May 2024):
  - Marked as compatible with 1.20.6
  - Improved Mod Menu integration
- 1.3.13 (2 May 2024): Marked as *incompatible* with 1.20.2-1.20.4
- 1.3.12 (2 May 2024):
  - Updated for 1.20.5
  - Moderate internal changes
- 1.3.11 (24 Jan. 2024): Updated for 1.20.2-1.20.4
- 1.3.10 (23 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.3.9 (20 Mar. 2023): Updated for 1.19.4
- 1.3.8 (6 Aug. 2022): Marked as compatible with 1.19.2
- 1.3.7 (28 Jul. 2022): 
  - Marked as compatible with 1.19.1
  - Minor internal changes
- 1.3.6 (25 Jul. 2022): Added recipes for rooted dirt and dripleaves
- 1.3.5 (5 Jul. 2022):
  - removed "anvil_crushing_" from the beginnings of builtin datapack names
  - log an error instead of throwing an exception in one case, fixing compatibility with Rug
- 1.3.4 (23 Jun. 2022): Updated for 1.19!
- 1.3.3 (14 Jun. 2022): Updated for 1.18.2
- 1.3.2-b1 (13 Dec. 2021): Marked as compatible with 1.18.1
- 1.3.1 (13 Dec. 2021):
  
  Marked as compatible with 1.18.1.

  No catastrophic failures have been reported, this is a full release (not beta)!

- 1.3.1-b1 (10 Dec. 2021): 
  
  Updated for 1.18!

  Improved tooltip wrapping.
  
- 1.3.0-b1 (13 Nov. 2021): This is a beta version
  - Compressed mod and datapack icons (thanks to [K0RR](https://gitlab.com/K0RR)!)
  - Added issues link to fabric.mod.json, accessible in Mod Menu (thanks to [K0RR](https://gitlab.com/K0RR)!)
  - Added initial [Roughly Enough Items (REI)](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-items) integration
    - block, ingredient, item, and loot drops are displayed separately
    - entity ingredients are currently represented using spawn egg items
    - if [Roughly Enough Resources (RER)](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-resources) isn't present, loot tables ids are displayed (not ideal)
    - if [RER](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-resources) is present, its method for displaying loot will be used
    - for [REI](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-items) and/or [RER](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-resources) features to work, all mods need to be installed on both client and server
    - [REI](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-items) and [RER](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-resources) are now both 'recommended' mods

- 1.2-1 (29 Oct. 2021): Lowered minimum Fabric Loader version to 0.11.7

- 1.2 (29 Oct. 2021):
  
  - Greatly simplified modifications to Minecraft's code (eliminated 99.9% of most common spaghetti!)
  - If a piston pushes a block into a falling anvil, the anvil will now pop into item form rather than crushing the block
  - datapack changes:
    - Default Anvil Crushing Recipes is now built-in (you can still disable it like you would any other datapack)
    - Split Default Dispenser Configurators into (all enabled by default):
      - anvil_crushing_block_breaking: makes anvils break weak blocks
      - anvil_crushing_block_degradation: makes anvils 'damage' many blocks they land on, turning them into less refined variants
      - anvil_crushing_stone_to_cobble: this is separate from block_degradation because it can override the new datapack compress_stone_to_deepslate
    - Added datapacks:
      - anvil_crushing_compress_stone_to_deepslate (default **dis**abled): Renewable deepslate! An anvil that crushes a pillar of three stone creates deepslate. This only works if stone_to_cobble is disabled
      - anvil_crushing_ice_compression: An anvil that crushes three ice creates packed ice; or if it crushes three packed ice, it creates blue ice. This is an alternative to crafting that works the same way the [carpet_extra's renewableIce](https://github.com/gnembon/carpet-extra#renewableice) feature does
      - anvil_crushing_crush_ores: Anvils cause the raw materials in ores to pop out into item form and degrade the ore block into cobble (or cobbled deepslate or netherrack)
      - anvil_crushing_wither_skele_on_basalt_to_blackstone: Renewable blackstone! An anvil that crushes a wither skeleton on basalt turns the basalt to blackstone
    - refined and fixed some of the existing datapacks, including:
      - heads are now crushed in block_breaking
      - deepslate variants are now degraded in block_degradation
  
  - JSON: strings can now define identifiers of ingredients ("#" at the start means it's a tag id)
  - various other internal clean-ups
  
- 1.1.2 (10 Jul. 2021): Marked as compatible with 1.17.1.
- 1.1.1 (17 Jun. 2021): Updated for 1.17 full release.
- 1.1 (28 Apr. 2021): 1.17 snapshot version (21w16a) Thanks to [siphalore](https://www.curseforge.com/members/siphalor/) for this update!
- 1.0 (16 Jan. 2021): First full release!
  Marked as compatible with 1.16.5. 
- 0.8.5-alpha (6 Jan. 2021): This is an ***alpha*** version. I made some non-trivial internal changes and may have overlooked something, introducing new issues. 
  
  Recipes now 'try harder' to place their block outputs by trying to place in each direction. 
  
  Fixed a bug where anvils would pop into item form after breaking a partial block with air below it. 
  
  Fixed a crash that occurred when falling anvils landed on moving blocks (pushed by pistons).
  
  Fixed an issue where bed block outputs that failed to place wouldn't drop a bed item. There're likely more issues like this related to failing to place multi-blocks, but unfortunately there's no universal fix. 

- 0.8.4-beta (11 Dec. 2020): Switched from using Minecraft's built-in recipe system to using a custom data type. 

  If you've made custom anvil crushing recipe datapacks, simply rename your `recipes` folder to `anvil_crushing_recipes`. 
  
  You may also remove the `"type": "anvil_crushing_recipes:anvil_crushing"` line, but leaving it in won't cause any problems. 
  
  Added a new recipe to 'Default Anvil Crushing Recipes': `cracked_nether_bricks_to_netherrack`.
- 0.8.3-alpha-3 (2 Dec. 2020): Hotfix for 'unexpected BlockEntity type' log spam reported in [issue 10](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/issues/10). 
- 0.8.3-alpha-2 (1 Dec. 2020): Hotfix for crash reported in [#9](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/issues/9). 
- 0.8.3-alpha-1 (11 Nov. 2020): This is an ***alpha*** version. I've made extensive changes internally, so it's more likely than normal that I've introduced new issues. 
  Marked as compatible with 1.16.4
  Added support for multiple inputs!
  Your old, single-ingredient recipes should still work
  Define multiple ingredients in the same way as before, but inside an array: `"ingredients": [...]`
  Upcoming change: in future versions (not this version) recipes will no longer reside in the `data/recipes` folder in datapacks; 
  instead they'll be in a dedicated directory along the lines of `data/anvil_crushing_recipes`. This is because anvil recipes 
  have so little in common with standard item recipes that it's become un-helpful and wasteful to use the vanilla recipe system. 
- 0.8.2 (17 Sep. 2020): Marked as compatible with 1.16.3, addressing [#6](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/issues/6)
- 0.8.1 (7 Sep. 2020): Fix [#5 Mobs drop no exp and no items](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/issues/5)
- 0.8.0 (6 Sep. 2020): New feature: entity crushing! 'Crushed' entities will be instantly killed. 
  - You can now specify `entity_ingredient` instead of `block_ingredient` in your recipes. `entity_ingredient`s may contain both entity entries and entity tags, similar to `block_ingredients`. 
  - **Important Change** to datapack format: `block_ingredient_drop_chance` is now `ingredient_drop_chance` to reflect that it will apply to either `block_ingredient` or `entity_ingredient`, whichever is specified. 
    If you have custom datapacks that used `block_ingredient_drop_chance`, you will need to update them to the new format. The 'Default Anvil Crushing Recipes' datapack has been updated. 
  - If `ingredient_drop_chance` is specified for an entity, it represents the chance that the entity will drop loot as though killed normally (it does not guarantee all possible drops will drop, they will have their normal random chances). 
    `ingredient_drop_chance` will only have affect mobs (`LivingEntity` internally), as they're the only entities that can have loot tables. 
  - New issues were likely introduced as a result of the
   -internal changes, bug reports will be appreciated. 