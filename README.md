<!--required for Modrinth centering -->
<center>
<div style="text-align:center;line-height:250%">

## Anvil Crushing Recipes

[![Minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_anvil-crushing-recipes_all.svg)](https://modrinth.com/mod/anvil-crushing-recipes/versions#all-versions)
![environment: server, opt client](https://img.shields.io/badge/environment-server,_opt_client-c65135)  
[![loader: Fabric](https://img.shields.io/badge/loader-Fabric-cdc4ae?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjYgMjgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48cGF0aCBkPSJtMTAgMjQtOC04TDE0IDRWMmgybDYgNnY0TDEwIDI0eiIgc3R5bGU9ImZpbGw6I2RiZDBiNCIvPjxwYXRoIGQ9Ik0xMiA2djJoLTJ2Mkg4djJINnYySDR2MkgydjRoMnYyaDJ2MmgydjJoMnYtMmgydi0yaDJ2LTJoMnYtMmgydi0yaDJ2LTRoLTJ2LTJoLTJWOGgtMlY2aDJ2MmgydjJoMnYyaDJ2Mmgydi0yaC0yVjhoLTJWNmgtMlY0aC0yVjJoMnYyaDJ2MmgydjJoMnYyaDJ2NGgtMnYyaC00djJoLTJ2MmgtMnYyaC0ydjRoLTJ2Mkg4di0ySDZ2LTJINHYtMkgydi0ySDB2LTRoMnYtMmgydi0yaDJ2LTJoMlY4aDJWNmgyVjJoMlYwaDJ2MmgtMnY0aC0yIiBzdHlsZT0iZmlsbDojMzgzNDJhIi8+PHBhdGggc3R5bGU9ImZpbGw6IzgwN2E2ZCIgZD0iTTIyIDEyaDJ2MmgtMnoiLz48cGF0aCBkPSJNMiAxOGgydjJoMnYyaDJ2MmgydjJIOHYtMkg2di0ySDR2LTJIMnYtMiIgc3R5bGU9ImZpbGw6IzlhOTI3ZSIvPjxwYXRoIGQ9Ik0yIDE2aDJ2MmgydjJoMnYyaDJ2Mkg4di0ySDZ2LTJINHYtMkgydi0yeiIgc3R5bGU9ImZpbGw6I2FlYTY5NCIvPjxwYXRoIGQ9Ik0yMiAxMnYtMmgtMlY4aC0yVjZsLTIuMDIzLjAyM0wxNiA4aDJ2MmgydjJoMnpNMTAgMjR2LTJoMnYtNGg0di00aDJ2LTJoMnY0aC0ydjJoLTJ2MmgtMnYyaC0ydjJoLTIiIHN0eWxlPSJmaWxsOiNiY2IyOWMiLz48cGF0aCBkPSJNMTQgMThoLTR2LTJIOHYtMmgydjJoNHYyem00LTRoLTR2LTJoLTJ2LTJoLTJWOGgydjJoMnYyaDR2MnpNMTQgNGgydjJoLTJWNHoiIHN0eWxlPSJmaWxsOiNjNmJjYTUiLz48L3N2Zz4=)](https://fabricmc.net/)
<a href="https://quiltmc.org/"><img alt="available for: Quilt Loader" width=73 src="https://raw.githubusercontent.com/QuiltMC/art/master/brand/1024png/quilt_available_dark.png"></a>  
<a href="https://modrinth.com/mod/fabric-api/versions"><img alt="Requires: Fabric API" width="60" src="https://i.imgur.com/Ol1Tcf8.png"></a>
[![supports: REI](https://img.shields.io/badge/supports-REI-2D2D2D)](https://modrinth.com/mod/rei/versions)
[![supports: Mod Menu](https://img.shields.io/badge/supports-Mod_Menu-134bfe?logo=data:image/webp+xml;base64,UklGRlIAAABXRUJQVlA4TEUAAAAv/8F/AA9wpv9T/M///McDFLeNpKT/pg8WDv6jiej/BAz7v+bbAKDn9D9l4Es/T/9TBr708/Q/ZeBLP0//c7Xiqp7z/QEA)](https://modrinth.com/mod/modmenu/versions)  
[![license: MIT](https://img.shields.io/badge/license-MIT-white)](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/blob/master/LICENSE)
[![source on: GitLab](https://img.shields.io/badge/source_on-GitLab-fc6e26?logo=gitlab)](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes)
[![issues: GitLab](https://img.shields.io/gitlab/issues/open-raw/supersaiyansubtlety/anvil_crushing_recipes?label=issues&logo=gitlab)](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/issues)
[![localized: Percentage](https://badges.crowdin.net/anvil-crushing-recipes/localized.svg)](https://crwd.in/anvil-crushing-recipes)  
[![Modrinth: Downloads](https://img.shields.io/modrinth/dt/anvil-crushing-recipes?logo=modrinth&label=Modrinth&color=00ae5d)](https://modrinth.com/mod/anvil-crushing-recipes/versions)
[![CurseForge: Downloads](https://img.shields.io/curseforge/dt/398523?logo=curseforge&label=CurseForge&color=f16437)](https://www.curseforge.com/minecraft/mc-mods/anvil-crushing-recipes/files)  
[![chat: Discord](https://img.shields.io/discord/1006391289006280746?logo=discord&color=5964f3)](https://discord.gg/xABmPngXAH)
<a href="https://coindrop.to/supersaiyansubtlety"><img alt="coindrop.to me" width="82" style="border-radius:3px" src="https://coindrop.to/embed-button.png"></a>
<a href="https://ko-fi.com/supersaiyansubtlety"><img alt="Buy me a coffee" width="106" src="https://i.ibb.co/4gwRR8L/p.png"></a>

</div>
</center>

---

### Define what happens when an anvil lands on blocks or entities. Data driven.

Works server-side and in single player.  
For [REI](https://modrinth.com/mod/rei/versions) support, it must be installed on the client as well.

Adds a new data type: `anvil_crushing_recipes:anvil_crushing`. Recipes can be added with data packs.

Recipes take one or more blocks, entities, or tags as input and can produce a block and/or a list of items as output.

<img alt="Match State" width=150 src="https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/raw/master/media/Anvil%20Crushing%20Recipes%20-%20Match%20State%20-%20Cropped.gif">
<img alt="Crush Entity" width=155 src="https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/raw/master/media/Anvil%20Crushing%20Recipes%20-%20Entity%20Recipe%20-%20Cropped.gif">

There are [more demonstration videos](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/tree/master/media)
on the repo.

---

<details>

<summary>Built in data packs</summary>

As of version 1.2 there are several built-in data packs (you can `/datapack disable` them like any other data pack):
- `anvil_crushing_block_breaking`, default: `enabled`
  > makes anvils break weak blocks [e.g. pumpkins and saplings]
- `anvil_crushing_block_degradation`, default: `enabled`
  > makes anvils 'damage' many blocks they land on, turning them into less refined variants [e.g. smooth stone to stone]
- `anvil_crushing_stone_to_cobble`, default: `enabled`
  > this is separate from block_degradation because it can override the new data pack `compress_stone_to_deepslate`
- `anvil_crushing_compress_stone_to_deepslate`, default: `disabled`
  > Renewable deepslate! An anvil that crushes a pillar of three stone creates deepslate. This only works if
`stone_to_cobble` is disabled
- `anvil_crushing_ice_compression`, default: `enabled`
  > An anvil that crushes three ice creates packed ice; or if it crushes three packed ice, it creates blue ice. This is
an alternative to crafting and works the same way the
[carpet_extra's renewableIce](https://github.com/gnembon/carpet-extra#renewableice) feature does
- `anvil_crushing_crush_ores`, default: `enabled`
  > Anvils cause the raw materials in ores to pop out into item form and degrade the ore block into cobble
(or cobbled deepslate or netherrack) [e.g. nether gold ore drops gold nuggets and becomes netherrack]
- `anvil_crushing_wither_skele_on_basalt_to_blackstone`, default: `enabled`
  > Renewable blackstone! An anvil that crushes a wither skeleton on basalt turns the basalt to blackstone

</details>

[SpaceEagle17](https://www.youtube.com/channel/UCAhYM1FGL0NQfbDbYUfIaZQ) has also created a data pack that makes anvils convert cobble to sand, which can be used in place of Carpet Extra's feature which does same thing. Download the data pack from [this page](https://www.planetminecraft.com/data-pack/cobblestone-to-sand-with-anvil-crushing-recipes/).

Both basic and detailed information on data pack format can be found on [the wiki](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/wikis/home). There are also lots of good examples in the built-in data packs. The format is as 'vanilla-like' as possible.

---

<details>

<summary>Translating</summary>

[![localized: Percentage](https://badges.crowdin.net/anvil-crushing-recipes/localized.svg)](https://crwd.in/anvil-crushing-recipes)  
If you'd like to help translate Anvil Crushing Recipes, you can do so on
[Crowdin](https://crwd.in/anvil-crushing-recipes).  

</details>

---

This mod is only for Fabric (works on Quilt, too!) and I won't be porting it to Forge. The license is MIT, however, so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
