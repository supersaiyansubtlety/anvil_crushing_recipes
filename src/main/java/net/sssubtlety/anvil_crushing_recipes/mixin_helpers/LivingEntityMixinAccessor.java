package net.sssubtlety.anvil_crushing_recipes.mixin_helpers;

import net.minecraft.entity.damage.DamageSource;

public interface LivingEntityMixinAccessor {
    void anvil_crushing_recipes$kill(DamageSource source, boolean drop);
}
