package net.sssubtlety.anvil_crushing_recipes.util;

import org.jetbrains.annotations.NotNull;

import java.util.*;

import static net.sssubtlety.anvil_crushing_recipes.util.Util.intAverage;

public class SortedCollection<T> implements Collection<T> {
    protected final List<T> array;
    protected final Set<T> set;
    protected final Comparator<T> comparator;

    public SortedCollection(Comparator<T> comparator) {
        this(new LinkedList<>(), comparator);
    }

    public SortedCollection(Collection<T> collection, Comparator<T> comparator) {
        this.array = new ArrayList<>(collection);
        this.set = new HashSet<>(collection);
        this.comparator = comparator;
    }

    @Override
    public int size() {
        return this.array.size();
    }

    @Override
    public boolean isEmpty() {
        return this.array.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return set.contains(o);
    }

    @Override
    public @NotNull Iterator<T> iterator() {
        return array.iterator();
    }

    @Override
    public Object @NotNull [] toArray() {
        return array.toArray();
    }

    @Override
    public <T1> T1 @NotNull [] toArray(T1[] a) {
        return array.toArray(a);
    }

    @Override
    public boolean add(T t) {
        if (this.contains(t)) return false;
        this.set.add(t);

        if (this.size() == 0)
            return this.array.add(t);


        int placement = findPlacement(t);
        if (comparator.compare(t, this.get(placement)) > 0)
            placement++;

        this.array.add(placement, t);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (!this.contains(o)) return false;

        set.remove(o);
        this.array.remove(this.findPlacement((T) o));

        return true;
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        return set.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean changed = false;
        for (T t : c)
            if (this.add(t)) changed = true;

        return changed;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        for (Object o : c)
            if (this.remove(o)) changed = true;

        return changed;
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        if (this.set.retainAll(c))
            return this.array.retainAll(c);

        return false;
    }

    @Override
    public void clear() {
        array.clear();
        set.clear();
    }

    public T get(int index) {
        if (index < 0 || index >= this.size()) return null;
        return array.get(index);
    }

    public T remove(int index) {
        set.remove(array.get(index));
        return array.remove(index);
    }

    public int indexOf(Object o) {
        return this.contains(o) ? findPlacement((T)o) : -1;
    }

    private int findPlacement(T t) {
        if (this.size() == 0) return 0;

        int begin = 0;
        int end = this.size() - 1;
        int mid = intAverage(begin, end);
        int difference = comparator.compare(this.get(mid), t);
        while(difference != 0) {
            if (difference < 0) {
                if (end == mid) break;
                end = mid;
            } else {
                if (begin == mid) break;
                begin = mid;
            }
            mid = intAverage(begin, end);
            difference = comparator.compare(this.get(mid), t);
        }

        return mid;
    }

//    private boolean shift(int index, int amount) {
//        if (index >= this.size() || amount == 0) return false;
//        if (amount < 0) amount = -amount;
//        for (int i = this.size() - 1; i >= index; i--) {
//            // shift element
//            this.array.add(i + amount, this.get(i));
//            // overwrite old spot with null
//            this.array.add(i, null);
//        }
//        return true;
//    }

    public SortedCollection<T> subList(int fromIndex, int toIndex) {
        return new SortedCollection<>(array.subList(fromIndex, toIndex), comparator);
    }
}
