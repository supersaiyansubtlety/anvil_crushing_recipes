package net.sssubtlety.anvil_crushing_recipes.util;

import net.fabricmc.fabric.api.resource.ResourcePackActivationType;
import net.fabricmc.loader.api.ModContainer;
import net.minecraft.block.BlockState;
import net.minecraft.block.FluidBlock;
import net.minecraft.util.Identifier;
import net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipes;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

import static net.fabricmc.fabric.api.resource.ResourceManagerHelper.registerBuiltinResourcePack;

public final class Util {
    private Util() { }

    public static int intAverage(int... integers) {
        int sum = 0;
        for (int integer : integers)
            sum += integer;

        return sum/integers.length;
    }

    public static boolean isEmptyState(BlockState state) {
        return state.isAir() || state.getBlock() instanceof FluidBlock;
    }

    public static void registerResourcePacks(ModContainer modContainer, ResourcePackActivationType activationType, String... paths) {
        for (String path : paths)
            registerBuiltinResourcePack(Identifier.of(AnvilCrushingRecipes.NAMESPACE, path), modContainer, activationType);
    }

    public static int ceilDiv(int dividend, int divisor) {
        return (int) Math.ceil(dividend / (float) divisor);
    }

    public static int greaterHalf(int value) {
        return ceilDiv(value, 2);
    }

    public static final class MaxInt {
        private int max;

        public MaxInt() {
            this(Integer.MIN_VALUE);
        }

        public MaxInt(int initialValue) {
            max = initialValue;
        }

        public static int getMax(List<Integer> ints) {
            final MaxInt max = new MaxInt();
            ints.forEach(max::update);
            return max.get();
        }

        public boolean update(int value) {
            if (value > max) {
                max = value;
                return true;
            } else return false;
        }

        public int get() {
            return max;
        }
    }

    public static final class DistinctBy<T, P> implements Predicate<T> {
        private final Function<T, P> getter;
        private final Set<P> seen;

        public DistinctBy(Function<T, P> getter) {
            this.getter = getter;
            this.seen = new HashSet<>();
        }

        @Override
        public boolean test(T t) {
            return seen.add(getter.apply(t));
        }
    }
}
