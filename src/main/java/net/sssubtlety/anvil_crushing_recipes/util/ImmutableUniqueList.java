package net.sssubtlety.anvil_crushing_recipes.util;

import com.google.common.collect.ForwardingList;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.*;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class ImmutableUniqueList<T> extends ForwardingList<T> {
    private final ImmutableList<T> delegate;
    private final ImmutableMap<T, Integer> map;

    protected ImmutableUniqueList(Builder<T> builder) {
        this(builder.list(), builder.map());
    }

    private ImmutableUniqueList(ImmutableList<T> delegate, ImmutableMap<T, Integer> map) {
        this.delegate = delegate;
        this.map = map;
    }

    @Override
    public int indexOf(Object value) {
        return map.getOrDefault(value, -1);
    }

    @Override
    public int lastIndexOf(Object value) {
        return indexOf(value);
    }

    @Override
    public boolean contains(Object value) {
        return map.containsKey(value);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        for (Object value : collection)
            if (!contains(value)) return false;

        return true;
    }

    @Override
    protected List<T> delegate() {
        return delegate;
    }

    public static <T> Builder<T> builder() {
        return new Builder<>();
    }

    public static class Builder<T> {
        protected final LinkedList<T> listBuilder;
        protected final HashMap<T, Integer> mapBuilder;

        public Builder() {
            this.listBuilder = new LinkedList<>();
            this.mapBuilder = new HashMap<>();
        }

        public boolean add(T value) {
            Integer index = mapBuilder.get(value);
            if (index != null) return false;
            mapBuilder.put(value, listBuilder.size());
            listBuilder.add(value);
            return true;
        }

        public boolean addAll(Iterable<T> values) {
            boolean added = false;
            for (T value : values)
                added |= add(value);

            return added;
        }

        public ImmutableUniqueList<T> build() {
            return new ImmutableUniqueList<>(list(), map());
        }

        protected ImmutableList<T> list() {
            return ImmutableList.copyOf(listBuilder);
        }

        protected ImmutableMap<T, Integer> map() {
            return ImmutableMap.copyOf(mapBuilder);
        }
    }

    @Override public void add(int index, T element) throws UnsupportedOperationException { super.add(index, element); }
    @Override public boolean addAll(int index, Collection<? extends T> elements) throws UnsupportedOperationException { return super.addAll(index, elements); }
    @Override public T remove(int index) throws UnsupportedOperationException { return super.remove(index); }
    @Override public T set(int index, T element) throws UnsupportedOperationException { return super.set(index, element); }
    @Override public boolean removeAll(Collection<?> collection) throws UnsupportedOperationException { return super.removeAll(collection); }
    @Override public boolean add(T element) throws UnsupportedOperationException { return super.add(element); }
    @Override public boolean remove(Object object) throws UnsupportedOperationException { return super.remove(object); }
    @Override public boolean addAll(Collection<? extends T> collection) throws UnsupportedOperationException { return super.addAll(collection); }
    @Override public boolean retainAll(Collection<?> collection) throws UnsupportedOperationException { return super.retainAll(collection); }
    @Override public void clear() throws UnsupportedOperationException { super.clear(); }
    @Override public void replaceAll(UnaryOperator<T> operator) throws UnsupportedOperationException { super.replaceAll(operator); }
    @Override public void sort(Comparator<? super T> c) throws UnsupportedOperationException { super.sort(c); }
    @Override public boolean removeIf(Predicate<? super T> filter) throws UnsupportedOperationException { return super.removeIf(filter); }
}
