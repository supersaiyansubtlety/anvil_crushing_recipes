package net.sssubtlety.anvil_crushing_recipes.util;

import com.google.common.collect.ImmutableSet;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipes;
import org.apache.commons.lang3.mutable.MutableInt;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

@Environment(EnvType.CLIENT)
public final class TextUtil {
    private TextUtil() { }

    public static final String DOT_NAMESPACE_DOT = "." + AnvilCrushingRecipes.NAMESPACE + ".";
    public static final Text ELLIPSES = Text.translatable("text" + DOT_NAMESPACE_DOT + "ellipses");

    private static final ImmutableSet<Character> PREFERRED_SPLIT_CHARS = ImmutableSet.of('_', '-');
    private static final int APPROX_CHAR_WIDTH = MinecraftClient.getInstance().textRenderer.getWidth("_");

    public static String fuzzyWrappedSentence(String sentence, int width) {
        return wrappedSentence(sentence, width, PREFERRED_SPLIT_CHARS);
    }

    private static String wrappedSentence(String sentence, int width, Set<Character> preferredSplitChars) {
        return wrapWords(splitSentence(sentence), width, preferredSplitChars);
    }

    // public static String wrappedSentence(String sentence, int width) {
    //     return wrapWords(splitSentence(sentence), width);
    // }

    @NotNull
    private static LinkedList<String> splitSentence(String sentence) {
        return Arrays.stream(sentence.split("((?= ))")).collect(Collectors.toCollection(LinkedList::new));
    }

    public static String fuzzyWrapWords(LinkedList<String> words, int maxWidth) {
        return wrapWords(words, maxWidth, PREFERRED_SPLIT_CHARS);
    }

    private static String wrapWords(LinkedList<String> words, int maxWidth, Set<Character> preferredSplitChars) {
        return wrapWordsImpl(words, maxWidth, (word, width) -> splitWord(word, width, preferredSplitChars));
    }

    // private static String wrapWords(LinkedList<String> words, int maxWidth) {
    //     return wrapWordsImpl(words, maxWidth, TextUtil::splitWord);
    // }

    @NotNull
    private static String wrapWordsImpl(LinkedList<String> words, int maxWidth, BiFunction<String, Integer, Pair<String, String>> wordSplitter) {
        StringBuilder lines = new StringBuilder();
        if (maxWidth <= APPROX_CHAR_WIDTH * 2) {
            // too narrow, don't bother
            for (String word : words)
                lines.append(word).append("\n");
            lines.deleteCharAt(lines.length() - 1);
            return lines.toString();
        }

        MutableInt curLineLen = new MutableInt(0);
        while (!words.isEmpty()) {
            lines.append(popNextWord(words, maxWidth, curLineLen, wordSplitter));
        }

        return lines.toString();
    }

    // public static Pair<String, String> fuzzySplitWord(String word, int width) {
    //     return splitWord(word, width, PREFERRED_SPLIT_CHARS);
    // }

    private static Pair<String, String> splitWord(String word, int width, Set<Character> preferredSplitChars) {
        final Pair<String, String> simpleSplit = splitWord(word, width);
        final String remainder = simpleSplit.getRight();
        if (remainder.isEmpty()) return simpleSplit;

        final String truncatedWord = simpleSplit.getLeft();
        int iBreak;
        for (iBreak = truncatedWord.length() - 1; iBreak > 0; iBreak--) {
            if (preferredSplitChars.contains(truncatedWord.charAt(iBreak))) {
                iBreak++;
                break;
            }
        }

        if (iBreak == 0) return simpleSplit;

        return new Pair<>(truncatedWord.substring(0, iBreak), truncatedWord.substring(iBreak) + remainder);
    }

    @NotNull
    private static Pair<String, String> splitWord(String word, int width) {
        if (MinecraftClient.getInstance().textRenderer.getWidth(word) <= width) return new Pair<>(word, "");

        int split = width / APPROX_CHAR_WIDTH;
        String truncatedWord;
        truncatedWord = word.substring(0, split);
        int approxWidth = MinecraftClient.getInstance().textRenderer.getWidth(truncatedWord);
        if (approxWidth != width) {
            if (approxWidth > width) {
                while (split > 0) {
                    split--;
                    truncatedWord = word.substring(0, split);
                    if (MinecraftClient.getInstance().textRenderer.getWidth(truncatedWord) <= width) break;
                }
            } else {
                int truncWordLen;
                do {
                    split++;
                    truncatedWord = word.substring(0, split);
                    truncWordLen = MinecraftClient.getInstance().textRenderer.getWidth(truncatedWord);
                    if (truncWordLen >= width) {
                        truncatedWord = truncatedWord.substring(0, --split);
                        break;
                    }
                } while(split <= truncWordLen);
            }
        }

        return new Pair<>(truncatedWord, word.substring(split));
    }

    private static String popNextWord(LinkedList<String> words, int maxWidth, MutableInt curLineLen, BiFunction<String, Integer, Pair<String, String>> wordSplitter) {
        String nextWord = words.removeFirst();

        final int wordLen = MinecraftClient.getInstance().textRenderer.getWidth(nextWord);
        if (curLineLen.getValue() + wordLen <= maxWidth) {
            curLineLen.add(wordLen);
            return nextWord;
        } else {
            String firstWordInLine = getWithoutLeadingSpace(nextWord);
            if (curLineLen.getValue() != 0) {
                firstWordInLine = "\n" + firstWordInLine;
                curLineLen.setValue(0);
                if (wordLen <= maxWidth) {
                    curLineLen.add(wordLen);
                    return firstWordInLine;
                }
            }
            // here curLineLen == 0 && wordLen > maxWidth
            Pair<String, String> splitWord = wordSplitter.apply(firstWordInLine, maxWidth);
            words.add(0, splitWord.getRight());
            return splitWord.getLeft() + "\n";
        }
    }

    private static String getWithoutLeadingSpace(String string) {
        if (string.charAt(0) == ' ') string = string.substring(1);
        return string;
    }

    public static String wrappedIdString(Identifier id, int maxWidth) {
        String idString = id.toString();
        if (MinecraftClient.getInstance().textRenderer.getWidth(idString) > maxWidth) {
            LinkedList<String> idPieces = new LinkedList<>();
            idPieces.add(id.getNamespace() + ":");
            idPieces.addAll(Arrays.asList(id.getPath().split("((?<=/))")));
            idString = fuzzyWrapWords(idPieces, maxWidth);
        }
        return idString;
    }

    public static Text truncatedIdText(Identifier id, int maxWidth) {
        String idString = id.toString();
        if (MinecraftClient.getInstance().textRenderer.getWidth(idString) > maxWidth) {
            idString = ELLIPSES.getString() + ":" + id.getPath();
            if (MinecraftClient.getInstance().textRenderer.getWidth(idString) > maxWidth) {
                final int lastSlashPos = idString.lastIndexOf('/');
                if (lastSlashPos != -1)
                    idString = ELLIPSES.getString() + idString.substring(lastSlashPos);

                idString = getConstrainedString(idString, maxWidth);
            }
        }
        return Text.of(idString);
    }

    private static String getConstrainedString(String string, int maxWidth) {
        if (MinecraftClient.getInstance().textRenderer.getWidth(string) > maxWidth) {
            int length = string.length();
            while (MinecraftClient.getInstance().textRenderer.getWidth(string + ELLIPSES.getString()) > maxWidth && length > 0) {
                length--;
                string = string.substring(0, length);
            }
            string += ELLIPSES.getString();
        }
        return string;
    }

    // public static void wrapSentences(String[] sentences) {
    //     for (int i = 0; i < sentences.length; i++)
    //         sentences[i] = wrappedSentence(sentences[i], REIUtils.getTooltipWidth());
    // }
}
