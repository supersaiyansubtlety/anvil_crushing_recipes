package net.sssubtlety.anvil_crushing_recipes.recipe;

import com.mojang.datafixers.util.Either;
import com.mojang.serialization.Codec;
import io.netty.buffer.ByteBuf;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.enums.BedPart;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.AutomaticItemPlacementContext;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContextParameterSet;
import net.minecraft.loot.context.LootContextType;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.registry.Registries;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.Property;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.sssubtlety.anvil_crushing_recipes.*;
import net.sssubtlety.anvil_crushing_recipes.mixin_helpers.LivingEntityMixinAccessor;
import net.sssubtlety.anvil_crushing_recipes.recipe.ingredient.BlockIngredient;
import net.sssubtlety.anvil_crushing_recipes.recipe.ingredient.GenericIngredient;
//import net.sssubtlety.anvil_crushing_recipes.util.AorB;
import net.sssubtlety.anvil_crushing_recipes.util.SortedCollection;
import net.sssubtlety.anvil_crushing_recipes.util.Util;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Stream;

import static net.minecraft.world.WorldEvents.BLOCK_BROKEN;
import static net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipes.LOGGER;
import static net.sssubtlety.anvil_crushing_recipes.util.Util.intAverage;

public class AnvilCrushingRecipe {
    public static final AnvilCrushingRecipeManager MANAGER =
        new AnvilCrushingRecipeManager("anvil_crushing_recipes");

    public static final Pair<Match, AnvilCrushingRecipe> NO_RECIPE = new Pair<>(Match.NONE, null);

    public static final Comparator<Object> OBJECT_HASH_COMPARATOR = Comparator.comparingInt(Object::hashCode);

    public static final Comparator<AnvilCrushingRecipe> ANVIL_CRUSHING_RECIPE_COMPARATOR = (left, right) -> {
        if (left.id.equals(right.id)) return 0;

        final int leftSize = left.ingredients.size();
        final int rightSize = right.ingredients.size();
        if (leftSize == rightSize) {
            for (int i = 0; i < leftSize; i++) {
                final int hashDiff = left.ingredients.get(i).hashCode() - right.ingredients.get(i).hashCode();
                if (hashDiff != 0) return hashDiff;
            }

            AnvilCrushingRecipes.LOGGER.error(
                "Found recipes with matching ingredients but different ids: \"{}\"; \"{}\"",
                left.id, right.id
            );

            return 0;
        } else return leftSize - rightSize;
    };

    private static final TreeMap<Block, SortedCollection<AnvilCrushingRecipe>> BLOCK_FIRST_RECIPES =
        new TreeMap<>(OBJECT_HASH_COMPARATOR);
    private static final TreeMap<EntityType<?>, SortedCollection<AnvilCrushingRecipe>> ENTITY_FIRST_RECIPES =
        new TreeMap<>(OBJECT_HASH_COMPARATOR);

    public final Identifier id;
    public final List<GenericIngredient<?>> ingredients;
    public final float ingredientDropChance;
    public final BlockItem blockOutput;
    public final List<ItemStack> itemOutputs;
    public final Identifier lootOutput;

    private LootTable lootTableOutputCache;

    // TODO:
    //  - add 'un-crafting' feature, by recipe type or by recipe name
    //  - add more parameters:
    //    - anvil speed
    //    - anvil fall distance
    //    - anvil damaged state
    //    - damage to anvil

    public AnvilCrushingRecipe(Identifier id, List<GenericIngredient<?>> ingredients, float ingredientDropChance, BlockItem blockOutput, List<ItemStack> itemOutputs, Identifier lootOutput) {
        this.id = id;
        this.ingredients = ingredients;
        this.ingredientDropChance = ingredientDropChance;
        this.blockOutput = blockOutput;
        this.itemOutputs = itemOutputs;
        this.lootOutput = lootOutput;
        this.lootTableOutputCache = null;
    }

    public static void updateAnvilCrushingRecipes(List<AnvilCrushingRecipe> anvilCrushingRecipes) {
        BLOCK_FIRST_RECIPES.clear();
        ENTITY_FIRST_RECIPES.clear();
        for (AnvilCrushingRecipe recipe : anvilCrushingRecipes)
            mapRecipe(recipe, recipe.ingredients.get(0));

        Networking.notifyListeners();
    }

    private static <T> void mapRecipe(AnvilCrushingRecipe recipe, GenericIngredient<T> firstIngredient) {
        TreeMap<T, SortedCollection<AnvilCrushingRecipe>> recipes = firstIngredient instanceof BlockIngredient ?
                (TreeMap<T, SortedCollection<AnvilCrushingRecipe>>) BLOCK_FIRST_RECIPES :
                (TreeMap<T, SortedCollection<AnvilCrushingRecipe>>) ENTITY_FIRST_RECIPES;
        for (T entry : firstIngredient.getMatches()) {
            SortedCollection<AnvilCrushingRecipe> matchingFirstIngredientRecipes = recipes.get(entry);
            if (matchingFirstIngredientRecipes == null) {
                matchingFirstIngredientRecipes = new SortedCollection<>(ANVIL_CRUSHING_RECIPE_COMPARATOR);
                recipes.put(entry, matchingFirstIngredientRecipes);
            }
            matchingFirstIngredientRecipes.add(recipe);
        }
    }

    public static Pair<Match, AnvilCrushingRecipe> getRecipeMatch(List<Either<BlockState, EntityType<?>>> crushed) {
        Either<BlockState, EntityType<?>> crushedFirst = crushed.get(0);
        SortedCollection<AnvilCrushingRecipe> possibleRecipes;
        if (crushedFirst.left().isPresent()) {
            Block block = crushedFirst.orThrow().getBlock();

            if (block == null) return NO_RECIPE;
            else possibleRecipes = BLOCK_FIRST_RECIPES.get(block);

        } else {
            possibleRecipes = ENTITY_FIRST_RECIPES.get(crushedFirst.swap().orThrow());
        }

        if (possibleRecipes == null || possibleRecipes.isEmpty()) return NO_RECIPE;

        AnvilCrushingRecipe firstRecipe = possibleRecipes.get(0);
        if (firstRecipe.ingredients.size() == 1)
            return new Pair<>(Match.FULL, firstRecipe);


        possibleRecipes = withMinIngredients(possibleRecipes, crushed.size());

        int numCrushed = crushed.size();
        int i;
        // loop through recipes with ingredients.size() == numCrushed
        // can only be FULL matches
        for (i = 0; i < possibleRecipes.size(); i++) {
            AnvilCrushingRecipe curRecipe = possibleRecipes.get(i);
            List<GenericIngredient<?>> curIngredients = curRecipe.ingredients;
            int curSize = curIngredients.size();
            if (curSize > numCrushed) break;
            int j;
            for (j = 1; j < curSize; j++) {
                Either<BlockState, EntityType<?>> curCrushed = crushed.get(j);
                if (curIngredients.get(j).misMatches(curCrushed.left().isPresent() ? curCrushed.orThrow().getBlock() : curCrushed.swap().orThrow())) break;
            }

            if (j == numCrushed) {
                return new Pair<>(Match.FULL, curRecipe);
            }
        }
        // loop through recipes with ingredients.size() > numCrushed
        // can only be PARTIAL matches
        for (; i < possibleRecipes.size(); i++) {
            AnvilCrushingRecipe curRecipe = possibleRecipes.get(i);
            int j;
            for (j = 1; j < numCrushed; j++) {
                Either<BlockState, EntityType<?>> curCrushed = crushed.get(j);
                if (curRecipe.ingredients.get(j).misMatches(curCrushed.left().isPresent() ? curCrushed.orThrow().getBlock() : curCrushed.swap().orThrow())) break;
            }

            if (j == numCrushed) return new Pair<>(Match.PARTIAL, curRecipe);
        }

        return NO_RECIPE;
    }

    public static List<Serialized> serializeRecipes() {
        return Stream.concat(
            streamDistinctRecipes(BLOCK_FIRST_RECIPES),
            streamDistinctRecipes(ENTITY_FIRST_RECIPES)
        )
            .map(Serialized::new)
            .toList();
    }

    // recipes contains one reference to a recipe for each block/entity in its first ingredient tag,
    //   this makes a stream with only one of each recipe
    private static <C extends Collection<AnvilCrushingRecipe>> Stream<AnvilCrushingRecipe> streamDistinctRecipes(
        TreeMap<?, C> recipes
    ) {
        return recipes.values().stream()
            .flatMap(Collection::stream)
            .filter(new Util.DistinctBy<>(recipe -> recipe.id));
    }

    public void output(BlockPos pos, World world, BlockState replacingState) {
        if (replacingState == null) placeOrDropBlockOutput(pos, world);
        else if (blockOutput != null) {
            if (!placeBlockMatchState(pos, world, replacingState))
                world.syncWorldEvent(null, BLOCK_BROKEN, pos, Block.getRawIdFromState(replacingState));
        }

        outputItems(pos, world);
        outputLoot(pos, world);
    }

    private static SortedCollection<AnvilCrushingRecipe> withMinIngredients(
        SortedCollection<AnvilCrushingRecipe> possibleRecipes, int min
    ) {
        if (possibleRecipes.isEmpty()) return possibleRecipes;

        int first = 0;
        int last = possibleRecipes.size() - 1;
        int mid = intAverage(first, last);

        //binary search
        while (true) {
            final int midSize = possibleRecipes.get(mid).ingredients.size();

            if (midSize == min) return possibleRecipes.subList(mid, possibleRecipes.size());
            else if (midSize > min) {
                if (last == mid) break;
                last = mid;
                mid = intAverage(first, last);
            }
            else {
                // midSize < min
                if (first == mid) break;
                first = mid;
                mid = intAverage(first, last);
            }
        }

        while (
            mid > 0 &&
            possibleRecipes.get(mid).ingredients.size() == possibleRecipes.get(mid - 1).ingredients.size()
        ) mid--;

        return possibleRecipes.subList(mid, possibleRecipes.size());
    }

    public void crushBlock(BlockPos pos, World world) {
        world.breakBlock(pos, shouldDropIngredient(world));
    }

    public void crushEntity(World world, Entity entity, Entity anvilEntity) {
        if (!entity.isAlive() || entity.isInvulnerable()) return;

        if (entity instanceof LivingEntity) {
            ((LivingEntityMixinAccessor) entity).anvil_crushing_recipes$kill(
                world.getDamageSources().fallingAnvil(anvilEntity),
                shouldDropIngredient(world)
            );
        } else entity.kill();
    }

    private void outputItems(BlockPos pos, World world) {
        if (itemOutputs != null)
            for (ItemStack stack : itemOutputs) Block.dropStack(world, pos, stack.copy());
    }

    private void outputLoot(BlockPos pos, World world) {
        final var lootOutput = getLootOutput(world.getServer());
        if (lootOutput != LootTable.EMPTY && world instanceof ServerWorld serverWorld)
            lootOutput.generateLoot(
                new LootContextParameterSet.Builder(serverWorld)
                    .build(new LootContextType.Builder().build())
            ).forEach(stack -> Block.dropStack(world, pos, stack.copy()));
    }

    private boolean shouldDropIngredient(World world) {
        return this.ingredientDropChance == 1 || (this.ingredientDropChance > 0 && world.random.nextFloat() < this.ingredientDropChance);
    }

    private boolean placeBlockMatchState(BlockPos pos, World world, BlockState oldState) {
        if (!placeOrDropBlockOutput(pos, world)) return false;

        BlockState newState = world.getBlockState(pos);
        final Block block = blockOutput.getBlock();
        if (oldState.getBlock().getClass().isInstance(block)) {
            // state is already in world from placeBlockOutputOrDropItsStacks
            // copy properties from old state to new state
            for (Property property : oldState.getProperties()) {
                Optional optionalValue = newState.getOrEmpty(property);
                if (optionalValue.isPresent()) {
                    Object value = optionalValue.get();
                    final Comparable oldValue = oldState.get(property);
                    if (value.getClass().isInstance(oldValue))
                        newState = newState.with(property, oldValue);
                }
            }
        }

        if (newState != null) world.setBlockState(pos, newState);

        return true;
    }

    // returns false iff stacks of blockOutput were dropped (because blockOutput couldn't be placed)
    private boolean placeOrDropBlockOutput(BlockPos pos, World world) {
        if (this.blockOutput == null) return true;
        if (!tryPlace(pos, world)) {
            //could not place block
            //drop stacks instead
//            //UNLESS we tried to place inside an anvil,
//            //then we assume we were pushed here by a piston, drop nothing
//            if (!(world.getBlockState(pos).getBlock() instanceof AnvilBlock))
            final var block = blockOutput.getBlock();
            final var defaultState = block.getDefaultState();
            // special handling for BedBlock because default state is foot which has no drops
            Block.dropStacks(
                block instanceof BedBlock ?
                    defaultState.with(BedBlock.PART, BedPart.HEAD) :
                    defaultState,
                world, pos
            );

            return false;
        }

        return true;
    }

    private boolean tryPlace(BlockPos pos, World world) {
        for (Direction direction : Direction.values()) {
            if (
                this.blockOutput.place(
                    new AutomaticItemPlacementContext(world, pos, direction, new ItemStack(blockOutput),
                    Direction.DOWN
                )) == ActionResult.CONSUME
            ) return true;
        }

        return false;
    }

    private LootTable getLootOutput(MinecraftServer server) {
        if (lootTableOutputCache == null) {
            if (lootOutput == null) lootTableOutputCache = LootTable.EMPTY;
            else {
                lootTableOutputCache =
                    server.method_58576().getLootTable(RegistryKey.of(RegistryKeys.LOOT_TABLE, lootOutput));

                if (lootTableOutputCache == LootTable.EMPTY)
                    LOGGER.error("Missing loot table: {}", lootOutput);
            }
        }

        return lootTableOutputCache;
    }

    // TODO:
    //  flatten blocks that share item (torch+wall_torch) to one recipe
    //  try pickblock if block.asItem is air
    public record Serialized(
        Identifier id,
        float ingredientDropChance,
        List<GenericIngredient<?>> ingredients,
        Optional<Identifier> blockOutput,
        Optional<List<ItemStack>> itemOutputs,
        Optional<Identifier> lootOutput
    ) {
        public static final PacketCodec<RegistryByteBuf, Serialized> CODEC = PacketCodec.tuple(
            Identifier.PACKET_CODEC,
            Serialized::id,

            PacketCodecs.FLOAT,
            Serialized::ingredientDropChance,

            PacketCodecs.factory(ArrayList::new, GenericIngredient.CODEC),
            Serialized::ingredients,

            PacketCodecs.optional(Identifier.PACKET_CODEC),
            Serialized::blockOutput,

            PacketCodecs.optional(PacketCodecs.factory(ArrayList::new, ItemStack.PACKET_CODEC)),
            Serialized::itemOutputs,

            PacketCodecs.optional(Identifier.PACKET_CODEC),
            Serialized::lootOutput,

            Serialized::new
        );

        public static final PacketCodec<RegistryByteBuf, List<Serialized>> LIST_CODEC =
            PacketCodecs.factory(ArrayList::new, Serialized.CODEC);

        public Serialized(AnvilCrushingRecipe recipe) {
            this(
                recipe.id,
                recipe.ingredientDropChance,
                recipe.ingredients,
                recipe.blockOutput == null ? Optional.empty() : Optional.of(Registries.ITEM.getId(recipe.blockOutput)),
                Optional.ofNullable(recipe.itemOutputs),
                Optional.ofNullable(recipe.lootOutput)
            );
        }

        // public static void toBuf(PacketByteBuf buf, Serialized serialized) {
        //     buf.writeIdentifier(serialized.id);
        //
        //     buf.writeFloat(serialized.ingredientDropChance);
        //
        //     buf.writeCollection(serialized.ingredients, GenericIngredient::toBuf);
        //
        //     buf.writeOptional(serialized.blockOutput, PacketByteBuf::writeIdentifier);
        //
        //     // buf.writeOptional(serializable.itemOutputs, (buf_, list) ->
        //     //     buf_.writeCollection(list, PacketByteBuf::writeItemStack)
        //     // );
        //
        //     ItemStack.field_51397.,
        //
        //     buf.writeOptional(serialized.lootOutput, PacketByteBuf::writeIdentifier);
        // }

        // public static Serialized fromBuf(PacketByteBuf buf) {
        //     return new Serialized(
        //         buf.readIdentifier(),
        //         buf.readFloat(),
        //         buf.readList(GenericIngredient::fromBuf),
        //         buf.readOptional(PacketByteBuf::readIdentifier),
        //         // buf.readOptional(buf_ -> buf_.readList(PacketByteBuf::readItemStack)),
        //         ItemStack.field_51397.,
        //         buf.readOptional(PacketByteBuf::readIdentifier)
        //     );
        // }

        private @Nullable BlockItem getBlockOut() {
            return blockOutput.map(id -> {
                final var item = Registries.ITEM.getOrEmpty(id);

                if (item.isPresent()) {
                    if (item.get() instanceof BlockItem blockItem) return blockItem;
                    else LOGGER.error("Client received non-BlockItem block output: {}", id);
                } else LOGGER.error("Client is missing block output item: {}", id);

                return null;
            }).orElse(null);
        }
    }

    @Environment(EnvType.CLIENT)
    public static final class ClientRecipe {
        public final Identifier id;
        public final List<Set<ItemStack>> ingredients;
        public final float ingredientDropChance;
        public final @Nullable Map<ItemStack, Identifier> finalIngredientLoot;
        public final @Nullable BlockItem blockOutput;
        public final @Nullable List<ItemStack> itemOutputs;
        public final @Nullable Identifier lootOutputId;

        public ClientRecipe(Serialized serialized) {
            this(
                serialized.id,
                serialized.ingredients.stream().map(GenericIngredient::getMatchingStacks).toList(),
                serialized.ingredientDropChance,
                // TODO null this if it won't drop
                serialized.ingredients.get(serialized.ingredients.size() - 1).getEntriesLoot(),
                serialized.getBlockOut(),
                serialized.itemOutputs.orElse(null),
                serialized.lootOutput.orElse(null)
            );
        }

        private ClientRecipe(
            Identifier id,
            List<Set<ItemStack>> ingredients,
            float ingredientDropChance,
            @Nullable Map<ItemStack, Identifier> finalIngredientLoot,
            @Nullable BlockItem blockOutput,
            @Nullable List<ItemStack> itemOutputs,
            @Nullable Identifier lootOutputId
        ) {
            this.id = id;
            this.ingredients = ingredients;
            this.ingredientDropChance = ingredientDropChance;
            this.finalIngredientLoot = finalIngredientLoot;
            this.blockOutput = blockOutput;
            this.itemOutputs = itemOutputs;
            this.lootOutputId = lootOutputId;
        }
    }

    public enum Match {
        FULL,
        PARTIAL,
        NONE
    }
}
