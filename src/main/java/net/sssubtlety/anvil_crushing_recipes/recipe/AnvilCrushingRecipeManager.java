package net.sssubtlety.anvil_crushing_recipes.recipe;

import com.google.gson.*;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.fabricmc.fabric.api.resource.ResourceReloadListenerKeys;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.registry.Registries;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.InvalidIdentifierException;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.profiler.Profiler;
import net.sssubtlety.anvil_crushing_recipes.recipe.ingredient.GenericIngredient;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

import static net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipes.LOGGER;
import static net.sssubtlety.anvil_crushing_recipes.recipe.AnvilCrushingRecipe.updateAnvilCrushingRecipes;
import static net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipes.ID;

public class AnvilCrushingRecipeManager extends JsonDataLoader implements IdentifiableResourceReloadListener {
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

    private static Map<Identifier, JsonElement> loader;

    public AnvilCrushingRecipeManager(String dataType) {
        super(GSON, dataType);
    }

    public static AnvilCrushingRecipe read(
        Identifier identifier, JsonObject json
    ) throws JsonSyntaxException, InvalidIdentifierException {
        ArrayList<GenericIngredient<?>> ingredients = new ArrayList<>();

        if (json.has(Keys.INGREDIENTS)) {
            final var ingredientArray = JsonHelper.getArray(json, Keys.INGREDIENTS);

            if (ingredientArray.isEmpty())
                throw new JsonSyntaxException("\"" + Keys.INGREDIENTS + "\" present but empty.");

            for (var ingredientElement : ingredientArray) {
                final var ingredientObject = ingredientElement.getAsJsonObject();
                if (ingredientObject.size() != 1) throw new JsonSyntaxException(
                    "\"" + Keys.INGREDIENTS + "\" must only contain objects with exactly one element."
                );
                // get first (only) ingredientObject entry and create GenericIngredient from it
                final var first = ingredientObject.entrySet().iterator().next();
                ingredients.add(GenericIngredient.fromJson(first.getValue(), first.getKey()));
            }
        } else {
            final var ingredient = GenericIngredient.find(json);

            if (ingredient == null)
                throw new JsonSyntaxException("An anvil crushing recipe must have at least one ingredient.");

            ingredients.add(ingredient);
        }

        final boolean dropChanceSpecified = json.has(Keys.INGREDIENT_DROP_CHANCE);

        float ingredientDropChance = dropChanceSpecified ?
            JsonHelper.getFloat(json, Keys.INGREDIENT_DROP_CHANCE) :
            0;

        final var blockOutput = AnvilCrushingRecipeManager.readBlockOutput(json);
        final var itemOutputs = readItemOutputs(json);
        final var lootOutputString = JsonHelper.getString(json, Keys.LOOT_OUTPUT, null);
        final var lootOutput = lootOutputString == null ?
            null :
            Identifier.parse(lootOutputString);

        // TODO: Allow all ingredients to drop
        //  allow current global drop chance or
        //  per-ingredient chance specification in object
        //  Also allow looting+silk touch (maybe arbitrary tool enchants)
        if (ingredients.size() == 1) {
            if (!dropChanceSpecified && blockOutput == null && itemOutputs == null && lootOutput == null)
                // if only one ingredient and no outputs of any are kind specified,
                // default to block_breaker
                ingredientDropChance = 1;
        } else if (dropChanceSpecified) {
            LOGGER.warn("Multiple ingredients specified, ignoring \"" + Keys.INGREDIENT_DROP_CHANCE + "\".");

            ingredientDropChance = 0;
        }

        return new AnvilCrushingRecipe(
            identifier,
            ingredients, ingredientDropChance,
            blockOutput, itemOutputs, lootOutput
        );
    }

    public static void postLoad() {
        // no recipes
        if (loader == null) return;

        final List<AnvilCrushingRecipe> recipes = new LinkedList<>();

        loader.forEach(((id, element) -> {
            if (element.isJsonObject()) {
                AnvilCrushingRecipe recipe;
                try {
                    recipe = read(id, element.getAsJsonObject());
                    recipes.add(recipe);
                } catch (JsonSyntaxException|InvalidIdentifierException e) {
                    LOGGER.error("Error in anvil crushing recipe \"{}\": ", id, e);
                }
            }
        }));

        updateAnvilCrushingRecipes(recipes);

        AnvilCrushingRecipeManager.loader = null;
    }

    private static ArrayList<ItemStack> readItemOutputs(JsonObject jsonObject) {
        JsonArray jsonArray = JsonHelper.getArray(jsonObject, Keys.ITEM_OUTPUTS, null);
        if (jsonArray == null) return null;

        ArrayList<ItemStack> itemOutputs = new ArrayList<>();
        jsonArray.forEach(element -> itemOutputs.add(toStack(element)));

        return itemOutputs.isEmpty() ? null : itemOutputs;
    }

    private static @NotNull ItemStack toStack(@NotNull JsonElement element) throws InvalidIdentifierException {
        final int count;
        final Identifier id;
        if (element.isJsonObject()) {
            final var object = element.getAsJsonObject();

            id = Identifier.parse(JsonHelper.getString(object, Keys.ITEM));
            count = JsonHelper.getInt(object, Keys.COUNT, 1);
        } else {
            id = Identifier.parse(JsonHelper.asString(element, Keys.ITEM_OUTPUTS));
            count = 1;
        }

        return Registries.ITEM.getOrEmpty(id)
            .map(item -> new ItemStack(item, count))
            .orElseThrow(() -> new JsonSyntaxException("Couldn't find \"" + Keys.ITEM_OUTPUTS + "\" item: " + id));
    }

    public static @Nullable BlockItem readBlockOutput(JsonObject jsonObject) throws InvalidIdentifierException {
        final var idString = JsonHelper.getString(jsonObject, Keys.BLOCK_OUTPUT, null);
        if (idString == null) return null;

        final var id = Identifier.parse(idString);
        final var airId = Registries.ITEM.getId(Items.AIR);
        if (id.equals(airId)) throw new JsonSyntaxException(
            "\"" + Keys.BLOCK_OUTPUT + "\" must not be \"" + airId + "\""
        );

        return Registries.ITEM.getOrEmpty(id).map(item -> {
            if (item instanceof BlockItem blockItem) return blockItem;
            else throw new JsonSyntaxException("\"" + Keys.BLOCK_OUTPUT + "\" is not a BlockItem: " + id);
        }).orElseThrow(() -> new JsonSyntaxException("Couldn't find \"" + Keys.BLOCK_OUTPUT + "\" item: " + id));
    }

    @Override
    public Identifier getFabricId() {
        return ID;
    }

    @Override
    public Collection<Identifier> getFabricDependencies() {
        return Collections.singleton(ResourceReloadListenerKeys.TAGS);
    }

    @Override
    protected void apply(Map<Identifier, JsonElement> loader, ResourceManager manager, Profiler profiler) {
        AnvilCrushingRecipeManager.loader = loader;
    }

    public interface Keys {
        String INGREDIENT_DROP_CHANCE = "ingredient_drop_chance";
        String ITEM_OUTPUTS = "item_outputs";
        String ITEM = "item";
        String COUNT = "count";
        String INGREDIENTS = "ingredients";
        String LOOT_OUTPUT = "loot_output";
        String BLOCK_OUTPUT = "block_output";
    }
}
