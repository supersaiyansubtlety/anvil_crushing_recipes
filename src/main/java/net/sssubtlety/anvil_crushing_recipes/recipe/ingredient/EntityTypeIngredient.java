package net.sssubtlety.anvil_crushing_recipes.recipe.ingredient;

import com.google.common.collect.ImmutableSet;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.registry.Registries;
import net.minecraft.util.Lazy;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class EntityTypeIngredient extends GenericIngredient<EntityType<?>> {
    @SuppressWarnings("deprecation")
    public static final EntityTypeIngredient EMPTY = new EntityTypeIngredient(
        ImmutableSet.of(),
        new Lazy<>(Collections::emptySet)
    );

    private static final String TYPE = "entity";
    private static final Provider<EntityType<?>> PROVIDER;

    private final ImmutableSet<EntityType<?>> matchingEntityTypes;

    @SuppressWarnings("deprecation")
    protected final Lazy<Set<ItemStack>> matchingStacks;

    static {
        PROVIDER = register(
            TYPE, Registries.ENTITY_TYPE, EntityTypeIngredient::of,
            EntityTypeIngredient::getItem, EntityType::getLootTableId
        );
    }

    @SuppressWarnings("deprecation")
    private EntityTypeIngredient(ImmutableSet<EntityType<?>> matchingEntityTypes, Lazy<Set<ItemStack>> matchingStacks) {
        this.matchingEntityTypes = matchingEntityTypes;
        this.matchingStacks = matchingStacks;
    }

    public static EntityTypeIngredient of(Collection<EntityType<?>> entities) {
        final var matchingEntityTypes = ImmutableSet.copyOf(entities);
        //noinspection deprecation
        return new EntityTypeIngredient(
            matchingEntityTypes,
            new Lazy<>(() -> matchingEntityTypes.stream()
                .map(entityType -> Optional.ofNullable(SpawnEggItem.forEntity(entityType)))
                .flatMap(Optional::stream)
                .map(ItemStack::new)
                // LinkedHashSet to maintain order
                .collect(Collectors.toCollection(LinkedHashSet::new))
            )
        );
    }

    private static SpawnEggItem getItem(EntityType<?> entityType) {
        return SpawnEggItem.forEntity(entityType);
    }

    @Override
    public Set<ItemStack> getMatchingStacks() {
        return matchingStacks.get();
    }

    @Override
    protected Provider<EntityType<?>> getProvider() {
        return PROVIDER;
    }

    @Override
    public Set<EntityType<?>> getMatches() {
        return matchingEntityTypes;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public boolean misMatches(Object candidate) {
        return !(candidate instanceof SpawnEggItem spawnEggItem) ||
                !this.test(spawnEggItem.getEntityType(null));
    }

    @Override
    public boolean test(EntityType<?> entityType) {
        return this.matchingEntityTypes.contains(entityType);
    }

    @Override
    public @NotNull Predicate<EntityType<?>> and(@NotNull Predicate<? super EntityType<?>> other) {
        return block -> test(block) && other.test(block);
    }

    @Override
    public @NotNull Predicate<EntityType<?>> negate() {
        return block -> !test(block);
    }
}
