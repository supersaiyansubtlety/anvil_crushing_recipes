package net.sssubtlety.anvil_crushing_recipes.recipe.ingredient;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.registry.Registries;
import net.minecraft.util.Lazy;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BlockIngredient extends GenericIngredient<Block> {
    @SuppressWarnings("deprecation")
    public static final BlockIngredient EMPTY =
        new BlockIngredient(ImmutableSet.of(), new Lazy<>(Collections::emptySet));

    private static final Provider<Block> PROVIDER;

    private final ImmutableSet<Block> matchingBlocks;

    @SuppressWarnings("deprecation")
    private final Lazy<Set<ItemStack>> matchingStacks;

    private static final String TYPE = "block";

    static {
        PROVIDER = register(
            TYPE, Registries.BLOCK, BlockIngredient::of,
            Block::asItem, Block::getLootTableId
        );
    }

    @SuppressWarnings("deprecation")
    private BlockIngredient(ImmutableSet<Block> matchingBlocks, Lazy<Set<ItemStack>> matchingStacks) {
        this.matchingBlocks = matchingBlocks;
        this.matchingStacks = matchingStacks;
    }

    public static BlockIngredient of(Collection<Block> blocks) {
        final ImmutableSet<Block> matchingBlocks = ImmutableSet.copyOf(blocks);
        //noinspection deprecation
        return new BlockIngredient(matchingBlocks, new Lazy<>(() ->
            matchingBlocks.stream().map(AbstractBlock::asItem)
            .filter(item -> item != Items.AIR)
            .map(ItemStack::new)
            // LinkedHashSet to maintain order
            .collect(Collectors.toCollection(LinkedHashSet::new)))
        );
    }

    @Override
    public Set<ItemStack> getMatchingStacks() {
        return matchingStacks.get();
    }

    @Override
    protected Provider<Block> getProvider() {
        return PROVIDER;
    }

    @Override
    public boolean misMatches(Object candidate) {
        //noinspection SuspiciousMethodCalls
        return !matchingBlocks.contains(candidate);
    }

    @Override
    public Set<Block> getMatches() {
        return matchingBlocks;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public boolean test(Block block) {
        return this.matchingBlocks.contains(block);
    }

    @Override
    public @NotNull Predicate<Block> and(@NotNull Predicate<? super Block> other) {
        return block -> test(block) && other.test(block);
    }

    @Override
    public @NotNull Predicate<Block> negate() {
        return block -> !test(block);
    }
}
