package net.sssubtlety.anvil_crushing_recipes.recipe.ingredient;

import com.google.gson.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.LootTable;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;
import net.minecraft.util.InvalidIdentifierException;
import net.minecraft.util.JsonHelper;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public abstract class GenericIngredient <T> implements Predicate<T> {
    // buf.writeString(this.getType());
    // buf.writeCollection(getMatchingSet(), (buf_, t) -> buf_.writeIdentifier(this.getProvider().registry.getId(t)));

    public static final PacketCodec<PacketByteBuf, GenericIngredient<?>> CODEC = PacketCodec.tuple(
        PacketCodecs.STRING,
        GenericIngredient::getType,
        PacketCodecs.factory(HashSet::new, Identifier.PACKET_CODEC),
        GenericIngredient::getMatchingIds,
        GenericIngredient::of
    );

    private static GenericIngredient<?> of(String type, Set<Identifier> ids) {
        return create(ids, providers.get(type));
    }

    // separate method to work around generic weirdness
    private static <T> GenericIngredient<T> create(Set<Identifier> ids, Provider<T> provider) {
        return provider.constructor.apply(
            ids.stream()
                .map(provider.registry::get)
                .toList()
        );
    }

    private static final Map<String, Provider<?>> providers = new LinkedHashMap<>();

    private static final String INGREDIENT_SUFFIX = "_ingredient";

    protected static <T> Provider<T> register(
        String type, Registry<T> registry, Function<Collection<T>, GenericIngredient<T>> constructor,
        Function<T, Item> getItem, Function<T, RegistryKey<LootTable>> getLoot
    ) {
        final var provider = new Provider<>(registry, constructor, getItem, getLoot);
        if (providers.put(type, provider) != null)
            throw new IllegalStateException("Attempting to re-register " + type + ".");

        return provider;
    }

    public static GenericIngredient<?> fromJson(
        JsonElement element, String key
    ) throws JsonSyntaxException, InvalidIdentifierException {
        final var type = removeIngredientSuffix(key);
        final var provider = providers.get(type);

        if (provider == null) throw new JsonSyntaxException("Unrecognized ingredient type: " + type);

        return fromJson(element, provider, type, key);
    }

    public static GenericIngredient<?> find(JsonObject json) throws JsonSyntaxException, InvalidIdentifierException {
        if (json.size() < providers.size()) {
            // check json entries to see if one has a key mapped in jsonReaders
            Set<Map.Entry<String, JsonElement>> entrySet = json.entrySet();
            for (Map.Entry<String, JsonElement> entry : entrySet) {
                final var key = entry.getKey();
                final var type = removeIngredientSuffix(key);
                if (type != null) return fromJson(entry.getValue(), providers.get(type), type, key);
            }
        } else {
            // check providers keys to see if one is in json
            for (Map.Entry<String, Provider<?>> entry : providers.entrySet()) {
                final var type = entry.getKey();
                final var key = type + INGREDIENT_SUFFIX;
                final var element = json.get(key);
                if (element != null) return fromJson(element, entry.getValue(), type, key);
            }
        }

        // not found
        return null;
    }

    // separate method to work around generic weirdness
    private static <T> GenericIngredient<T> fromJson(
        JsonElement element, Provider<T> provider,
        String type, String key
    ) throws JsonSyntaxException, InvalidIdentifierException {
        return provider.constructor.apply(getEntries(element, type, provider.registry, key));
    }

    @NotNull
    private static <T> List<T> getEntries(
        JsonElement element, String type, Registry<T> registry, String key
    ) throws JsonSyntaxException, InvalidIdentifierException {
        if (element == null || element.isJsonNull())
            throw new JsonSyntaxException("\"" + key + "\" must not be empty.");

        if (element.isJsonArray()) {
            final var array = element.getAsJsonArray();
            if (array.isEmpty()) {
                throw new JsonSyntaxException("\"" + key + "\" array must not be empty.");
            } else {
                return StreamSupport.stream(array.spliterator(), false)
                    .flatMap(arrayElement -> getFlatEntries(arrayElement, registry, type, key).stream())
                    .collect(Collectors.toCollection(ArrayList::new));
            }
        } else return getFlatEntries(element, registry, type, key);
    }

    @NotNull
    private static <T> List<T> getFlatEntries(
        JsonElement element, Registry<T> registry, String type, String key
    ) throws JsonSyntaxException, InvalidIdentifierException {
        if (element.isJsonObject()) {
            final JsonObject object = element.getAsJsonObject();
            if (JsonHelper.hasElement(object, type)) {
                return getSingleEntry(registry, key, JsonHelper.getString(object, type));
            } else {
                return getTagEntries(registry, key, JsonHelper.getString(object, "tag"));
            }
        } else if (element.isJsonPrimitive()) {
            final var primitive = element.getAsJsonPrimitive();
            if (primitive.isString()) {
                String id = primitive.getAsString();
                if (id.startsWith("#")) {
                    // tag
                    return getTagEntries(registry, key, id.substring(1));
                } else {
                    // entry, not tag
                    return getSingleEntry(registry, key, id);
                }
            }
        }

        throw new JsonSyntaxException(
            "Expected element to be string, object, or array of objects, but was " + JsonHelper.getType(element)
        );
    }

    @NotNull
    private static <T> List<T> getSingleEntry(
        Registry<T> registry, String key, String id
    ) throws JsonSyntaxException, InvalidIdentifierException {
        final var entry = registry.getOrEmpty(Identifier.parse(id));
        if (entry.isPresent()) return new ArrayList<>(List.of(entry.get()));
        else throw new JsonSyntaxException("\"" + key + "\" entry is missing: " + id);
    }

    @NotNull
    private static <T> List<T> getTagEntries(
        Registry<T> registry, String key, String id
    ) throws JsonSyntaxException, InvalidIdentifierException {
        final var tagKey = TagKey.of(registry.getKey(), Identifier.parse(id));

        if (tagKey == null) throw new JsonSyntaxException("\"" + key + "\" tag entry is missing: " + id);

        final var tagItr = registry.getTagOrEmpty(tagKey).iterator();
        if (!tagItr.hasNext())
            throw new JsonSyntaxException("\"" + key + "\" tag entry is empty: " + id);

        final List<T> values = new ArrayList<>();
        tagItr.forEachRemaining(holder -> values.add(holder.value()));

        return values;
    }

    private static String removeIngredientSuffix(String key) {
        if (!key.endsWith(INGREDIENT_SUFFIX)) return null;
        return key.substring(0, key.length() - INGREDIENT_SUFFIX.length());
    }

    public static GenericIngredient<?> fromBuf(PacketByteBuf buf) {
        return create(providers.get(buf.readString()), buf.readList(PacketByteBuf::readIdentifier));
    }

    // separate method to work around generic weirdness
    private static <T> GenericIngredient<?> create(Provider<T> provider, List<Identifier> matchingIds) {
        return provider.constructor.apply(matchingIds.stream().map(provider.registry::get).toList());
    }

    public static void toBuf(PacketByteBuf buf, GenericIngredient<?> ingredient) {
        // write type so it can be used to get provider
        ingredient.toBuf(buf);
    }

    // separate instance method to work around generic weirdness
    private void toBuf(PacketByteBuf buf) {
        // write type so it can be used to get provider
        buf.writeString(this.getType());

        buf.writeCollection(getMatches(), (buf_, t) -> buf_.writeIdentifier(this.getProvider().registry.getId(t)));
    }

    public final void init() { }

    public Map<ItemStack, Identifier> getEntriesLoot() {
        final Map<ItemStack, Identifier> entriesLoot = new HashMap<>();
        for (T t : getMatches()) {
            Item item = getProvider().getItem.apply(t);
            if (item != Items.AIR) entriesLoot.put(new ItemStack(item), getProvider().getLoot.apply(t).getValue());
        }

        return entriesLoot;
    }

    public abstract String getType();

    public abstract Set<T> getMatches();

    public abstract Set<ItemStack> getMatchingStacks();

    public final Set<Identifier> getMatchingIds() {
        return getMatches().stream()
            .map(t -> getProvider().registry.getId(t))
            .collect(Collectors.toSet());
    }

    public abstract boolean misMatches(Object candidate);

    protected abstract Provider<T> getProvider();

    protected static final class Provider<T> {
        private final Registry<T> registry;
        private final Function<Collection<T>, GenericIngredient<T>> constructor;
        private final Function<T, Item> getItem;
        private final Function<T, RegistryKey<LootTable>> getLoot;

        private Provider(
            Registry<T> registry, Function<Collection<T>, GenericIngredient<T>> constructor,
            Function<T, Item> getItem, Function<T, RegistryKey<LootTable>> getLoot
        ) {
            this.constructor = constructor;
            this.registry = registry;
            this.getItem = getItem;
            this.getLoot = getLoot;
        }

        public GenericIngredient<T> create(Collection<T> collection) {
            return constructor.apply(collection);
        }
    }
}
