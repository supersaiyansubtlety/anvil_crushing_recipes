package net.sssubtlety.anvil_crushing_recipes.rei.rer;

import com.google.common.collect.ImmutableMap;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.widgets.Slot;
import me.shedaniel.rei.api.common.entry.EntryStack;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.sssubtlety.anvil_crushing_recipes.util.Util;
import net.sssubtlety.anvil_crushing_recipes.rei.ReiUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static net.sssubtlety.anvil_crushing_recipes.util.TextUtil.DOT_NAMESPACE_DOT;

public class EntrySyncedLootWidget extends AbstractLootWidget {
    private static final Text LOOT_TABLES =
        Text.translatable("tooltip" + DOT_NAMESPACE_DOT + "loot_tables_prefix");

    private final Slot ingredientSlot;
    private final ImmutableMap<EntryStack<?>, LootWidget> ingredientEntries2LootWidgetMap;
    private final Rectangle bounds;
    public final Text tooltip;

    public EntrySyncedLootWidget(Rectangle bounds, Slot ingredientSlot, ImmutableMap<EntryStack<?>, Identifier> ingredientEntries2LootIdMap) {
        this.ingredientSlot = ingredientSlot;
        Pair<ImmutableMap<EntryStack<?>, LootWidget>, String> entry2LootMapAndTooltip = getEntry2LootMapAndTooltip(ingredientEntries2LootIdMap, bounds);
        this.ingredientEntries2LootWidgetMap = entry2LootMapAndTooltip.getLeft();
        this.tooltip = Text.literal(entry2LootMapAndTooltip.getRight());
        this.bounds = bounds;
    }

    private static Pair<ImmutableMap<EntryStack<?>, LootWidget>, String> getEntry2LootMapAndTooltip(ImmutableMap<EntryStack<?>, Identifier> entry2LootIdMap, Rectangle bounds) {
        ImmutableMap.Builder<EntryStack<?>, LootWidget> mapBuilder = ImmutableMap.builder();
        Util.MaxInt maxWidth = new Util.MaxInt(0);
        final StringBuilder tooltipBuilder = new StringBuilder(LOOT_TABLES.getString()).append("\n");
        for (Map.Entry<EntryStack<?>, Identifier> entry2IdMapping : entry2LootIdMap.entrySet()) {
            Identifier id = entry2IdMapping.getValue();

            tooltipBuilder.append(ReiUtils.tooltipIdString(id)).append("\n");

            final LootWidget lootWidget = new LootWidget(id, bounds, null);
            maxWidth.update(lootWidget.getBounds().width);
            mapBuilder.put(entry2IdMapping.getKey(), lootWidget);
        }
        // remove trailing newline
        tooltipBuilder.deleteCharAt(tooltipBuilder.length() - 1);

        bounds.width = maxWidth.get();
        return new Pair<>(mapBuilder.build(), tooltipBuilder.toString());
    }

    public LootWidget getLootWidget() {
        return ingredientEntries2LootWidgetMap.get(ingredientSlot.getCurrentEntry());
    }

    @Override
    public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
        getLootWidget().render(graphics, mouseX, mouseY, delta);
    }

    public void translate(int dx, int dy) {
        super.translate(dx, dy);
        ingredientEntries2LootWidgetMap.values().forEach(widgetContainer -> widgetContainer.translate(dx, dy));
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }

    @Override
    public List<? extends Element> children() {
        return new LinkedList<>(this.ingredientEntries2LootWidgetMap.values());
    }

    @Override
    public Text getTooltip() {
        return tooltip;
    }
}
