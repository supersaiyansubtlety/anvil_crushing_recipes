package net.sssubtlety.anvil_crushing_recipes.rei.rer;

import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.widgets.WidgetWithBounds;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.sssubtlety.anvil_crushing_recipes.rei.ReiUtils;
import uk.me.desert_island.rer.rei_stuff.LootOutput;

import java.util.List;

import static net.sssubtlety.anvil_crushing_recipes.rei.AnvilCrushingCategory.Measurements.SLOT_SIDE_LEN;
import static net.sssubtlety.anvil_crushing_recipes.util.TextUtil.DOT_NAMESPACE_DOT;

public class LootWidget extends AbstractLootWidget {
    private static final Text LOOT_TABLE_PREFIX =
        Text.translatable("tooltip" + DOT_NAMESPACE_DOT + "loot_table_prefix");

    private final List<WidgetWithBounds> children;
    private final Rectangle bounds;
    private final Text tooltip;

    public static LootWidget create(Identifier lootTableId, Rectangle bounds) {
        Text tooltipString = Text.literal(LOOT_TABLE_PREFIX.getString() + "\n" + ReiUtils.tooltipIdString(lootTableId));
        return new LootWidget(lootTableId, bounds, tooltipString);
    }

    protected LootWidget(Identifier lootTableId, Rectangle bounds, Text tooltip) {
        final Pair<List<WidgetWithBounds>, Rectangle> lootWidgetsAndBounds = getWidgetsAndBounds(lootTableId, bounds);
        this.children = lootWidgetsAndBounds.getLeft();
        this.bounds = lootWidgetsAndBounds.getRight();
        this.tooltip = tooltip;
    }

    private static Pair<List<WidgetWithBounds>, Rectangle> getWidgetsAndBounds(Identifier lootTableId, Rectangle bounds) {
        final SimpleLootWithId simpleLootWithId = new SimpleLootWithId(lootTableId);
        final List<LootOutput> outputs = simpleLootWithId.getOutputs();
        simpleLootWithId.outputs = outputs;

        final int numOutputs = outputs.size();
        final int maxWidth = bounds.width;
        final Rectangle adjustedBounds = bounds.clone();
        adjustedBounds.width = Math.max(1, numOutputs) * SLOT_SIDE_LEN + 7;
        if (adjustedBounds.width > maxWidth) {
            adjustedBounds.width = maxWidth - maxWidth % SLOT_SIDE_LEN + 7;
            if (adjustedBounds.width > maxWidth) adjustedBounds.width -= SLOT_SIDE_LEN;
        }

        final List<WidgetWithBounds> widgets = (List<WidgetWithBounds>) (List<?>) CATEGORY_INSTANCE.setupDisplay(simpleLootWithId, adjustedBounds);
        widgets.remove(widgets.size() - 1);

        return new Pair<>(widgets, adjustedBounds);
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }

    @Override
    public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
        children.forEach(child -> child.render(graphics, mouseX, mouseY, delta));
    }

    @Override
    public List<? extends Element> children() {
        return children;
    }

    @Override
    public void translate(int dx, int dy) {
        children.forEach(child -> child.getBounds().translate(dx / 2, dy / 2));
        this.bounds.translate(dx, dy);
    }

    @Override
    public Text getTooltip() {
        return tooltip;
    }
}
