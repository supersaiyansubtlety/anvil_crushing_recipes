package net.sssubtlety.anvil_crushing_recipes.rei.rer;

import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.widgets.Widget;
import me.shedaniel.rei.api.common.entry.EntryIngredient;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.ParentElement;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.sssubtlety.anvil_crushing_recipes.rei.AnvilCrushingPlugin;
import net.sssubtlety.anvil_crushing_recipes.rei.widgets.MovableWidget;
import uk.me.desert_island.rer.rei_stuff.LootCategory;
import uk.me.desert_island.rer.rei_stuff.LootDisplay;
import uk.me.desert_island.rer.rei_stuff.LootOutput;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractLootWidget extends MovableWidget.WithBounds implements ParentElement {
    protected static final LootCategory CATEGORY_INSTANCE = new MinimalLootCategory();

    static {
        if (!AnvilCrushingPlugin.RER_LOADED) throw new IllegalStateException("AbstractLootWidget class initializing but RER is not loaded!");
    }

    @Override
    public boolean mouseScrolled(double mouseX, double mouseY, double amount, double d) {
        boolean scrolled = false;
        for (Element child : children())
            scrolled |= child.mouseScrolled(mouseX, mouseY, amount, d);

        return scrolled;
    }


    protected static class MinimalLootCategory extends LootCategory {
        @Override
        protected void registerWidget(LootDisplay display, List<Widget> widgets, Rectangle bounds) { }

        @Override
        protected Rectangle getOutputsArea(Rectangle root) {
            return root.clone();
        }
    }

    public abstract Text getTooltip();

    public static List<EntryIngredient> lootIdToEntryIngredient(Identifier lootId) {
        return new SimpleLootWithId(lootId).getOutputEntries();
    }

    protected static class SimpleLootWithId extends LootDisplay {
        public SimpleLootWithId(Identifier lootTableId) {
            this.lootTableId = lootTableId;
        }

        @Override
        public Identifier getLocation() {
            return lootTableId;
        }

        @Override
        public List<LootOutput> getOutputs() {
            if (this.outputs == null) {
                // filter out outputs with complex conditions, because an anvil usually won't meet those conditions
                this.outputs = super.getOutputs().stream()
                        .filter(lootOutput -> lootOutput.extraText == null || lootOutput.extraText.length() == 0)
                        .collect(Collectors.toList());
            }

            return this.outputs;
        }
    }
}
