package net.sssubtlety.anvil_crushing_recipes.rei;

import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.Renderer;
import me.shedaniel.rei.api.client.gui.widgets.Slot;
import me.shedaniel.rei.api.client.gui.widgets.Widget;
import me.shedaniel.rei.api.client.gui.widgets.WidgetWithBounds;
import me.shedaniel.rei.api.client.gui.widgets.Widgets;
import me.shedaniel.rei.api.client.registry.display.DisplayCategory;
import me.shedaniel.rei.api.common.category.CategoryIdentifier;
import me.shedaniel.rei.api.common.entry.EntryIngredient;
import me.shedaniel.rei.api.common.entry.EntryStack;
import me.shedaniel.rei.api.common.util.EntryStacks;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.text.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.sssubtlety.anvil_crushing_recipes.util.Util;
import net.sssubtlety.anvil_crushing_recipes.rei.rer.EntrySyncedLootWidget;
import net.sssubtlety.anvil_crushing_recipes.rei.rer.LootWidget;
import net.sssubtlety.anvil_crushing_recipes.rei.widgets.*;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static net.sssubtlety.anvil_crushing_recipes.util.TextUtil.DOT_NAMESPACE_DOT;
import static net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipes.NAMESPACE;
import static net.sssubtlety.anvil_crushing_recipes.util.Util.greaterHalf;
import static net.sssubtlety.anvil_crushing_recipes.util.TextUtil.fuzzyWrappedSentence;
import static net.sssubtlety.anvil_crushing_recipes.rei.AnvilCrushingCategory.Measurements.*;
import static net.sssubtlety.anvil_crushing_recipes.rei.AnvilCrushingCategory.Texts.*;
import static net.sssubtlety.anvil_crushing_recipes.rei.ReiUtils.*;

@Environment(EnvType.CLIENT)
public class AnvilCrushingCategory implements DisplayCategory<AnvilCrushingRecipeDisplay> {
    public static final EntryStack<ItemStack> ICON = EntryStacks.of(Blocks.ANVIL);

    public static final CategoryIdentifier<AnvilCrushingRecipeDisplay> ID =
        CategoryIdentifier.of(NAMESPACE, "plugins/anvil_crushing");

    private static final ImmutablePoint ORIGIN = new ImmutablePoint();

    @Override
    public Renderer getIcon() {
        return ICON;
    }

    @Override
    public Text getTitle() {
        return TITLE;
    }

    @Override
    public List<Widget> setupDisplay(AnvilCrushingRecipeDisplay display, Rectangle bounds) {
        return (List<Widget>)(List<? extends Widget>) setupDisplayImpl(display,bounds);
    }

    protected <T extends WidgetWithBounds & MovableWidget> List<T> setupDisplayImpl(
        AnvilCrushingRecipeDisplay display, Rectangle bounds
    ) {
        final int minX = bounds.x;
        final int centerY = bounds.getCenterY();

        final List<T> widgets = new ArrayList<>();

        final List<EntryIngredient> inputs = display.getInputEntries();
        final int size = inputs.size();
        final int inputColumnHeight = COLUMN_SPACING * (size - 1) + SLOT_SIDE_LEN;
        final int inputColumnTopHalf = greaterHalf(inputColumnHeight);

        Point currentPoint = new Point(minX, centerY - inputColumnTopHalf);

        // anvil above column of inputs, excluded when vertically centering column
        Point anvilPos = currentPoint.clone();
        anvilPos.y -= SLOT_SIDE_LEN;
        widgets.add(makeAnvilWidget(anvilPos));

        // slot base for column of inputs
        widgets.add(new MovableSlotBase(new Rectangle(
            currentPoint.x, currentPoint.y,
            SLOT_SIDE_LEN, inputColumnHeight
        )).cast());
        // createSlot translates it slots to account for their border, this translation accommodates that
        currentPoint.translate(1, 1);

        MovableSlotWidget finalInputSlot = null;
        for (EntryIngredient input : inputs) {
            finalInputSlot = new MovableSlotWidget(currentPoint).entries(input);
            finalInputSlot.disableBackground();
            finalInputSlot.disableHighlight();
            widgets.add(finalInputSlot.cast());
            currentPoint.translate(0, COLUMN_SPACING);
        }
        // re-center vertically, then move back up to center arrow
        currentPoint.y = centerY - MovableArrow.ARROW_TOP_HALF_HEIGHT;
        // move right by input column width + padding, to where arrow should start
        currentPoint.x += SLOT_SIDE_LEN + H_PADDING;

        final MovableArrow arrow = new MovableArrow(currentPoint);
        arrow.tooltip(FROM_PREFIX.copy().append("\n").append(tooltipIdString(display.id)));
        widgets.add(arrow.cast());

        final int widthLeftOfArrowCenter = currentPoint.x - minX + MovableArrow.ARROW_HALF_WIDTH;

        currentPoint.x += MovableArrow.ARROW_WIDTH + H_PADDING + 2;
        currentPoint.y = centerY;

        final int maxX = bounds.getMaxX();
        final Pair<List<T>, Integer> outputWidgetsAndRightX =
            getOutputWidgetsAndMaxX(display, new ImmutablePoint(currentPoint), maxX, finalInputSlot);
        widgets.addAll(outputWidgetsAndRightX.getLeft());

        final int xTranslation = maxX - outputWidgetsAndRightX.getRight();
        if (xTranslation > 0) widgets.forEach(widget ->
            widget.translate(Math.min(xTranslation, widthLeftOfArrowCenter), 0)
        );

        return widgets;
    }

    private <T extends WidgetWithBounds & MovableWidget> T makeAnvilWidget(Point point) {
        final var anvilWidget = new MovableRendererWithBounds(
            new Rectangle(point.x, point.y, SLOT_SIDE_LEN, SLOT_SIDE_LEN),
            ICON
        ).withTooltip(Text.of(fuzzyWrappedSentence(ANVIL_TOOLTIP.getString(), getTooltipWidth())));

        return anvilWidget.cast();
    }

    // TODO: make block output slot 'output slot sized'
    private <T extends WidgetWithBounds & MovableWidget> Pair<List<T>, Integer> getOutputWidgetsAndMaxX(
        AnvilCrushingRecipeDisplay display, ImmutablePoint centerLeft, int maxAllowedX, Slot finalInputSlot
    ) {
        // Block output
        // ingredient loot
        // item output
        // loot

        final List<T> outputWidgets = new ArrayList<>();
        final var currentPoint = centerLeft.clone();

        if (display.blockOutput != null) {
            final var blockSlot = new MovableOutputSlotWidget(currentPoint).entry(EntryStacks.of(display.blockOutput));
            final int halfBlockSlotSideLen = greaterHalf(blockSlot.getBounds().width) - H_PADDING;
            currentPoint.y -= halfBlockSlotSideLen + 5;
            blockSlot.translate(0, -halfBlockSlotSideLen);
            currentPoint.x += halfBlockSlotSideLen;
            outputWidgets.add(blockSlot.cast());

            final var blockOutputLabel = new MovableLabel(currentPoint.clone(), BLOCK_OUTPUT_LABEL);
            blockOutputLabel.tooltip(BLOCK_OUTPUT_TOOLTIP);

            blockOutputLabel.getPoint().y -= blockOutputLabel.getBounds().height;
            outputWidgets.add(blockOutputLabel.cast());

            final int blockOutputWidth = Math.max(
                halfBlockSlotSideLen,
                blockOutputLabel.getBounds().getMaxX() - blockOutputLabel.getX()
            ) + H_PADDING;
            // move point to right of block output
            currentPoint.x += blockOutputWidth;
            // re-center vertically
            currentPoint.y = centerLeft.y;
        }

        final int minX = currentPoint.x;
        final int maxAllowedWidth = maxAllowedX - minX;

        final var maxWidth = new Util.MaxInt(0);

        final List<T> additionalOutputs = new ArrayList<>();
        int additionalOutputTypeCount = 0;
        if (tryAddIngredientLoot(
            display,
            currentPoint, maxWidth, maxAllowedWidth,
            finalInputSlot, additionalOutputs
        )) additionalOutputTypeCount++;

        if (display.itemOutputs != null) {
            additionalOutputTypeCount++;
            addItemsOutput(
                currentPoint, minX, Math.max(1, maxAllowedWidth / SLOT_SIDE_LEN), maxWidth,
                additionalOutputs, display.itemOutputs
            );
        }

        if (display.lootOutputId != null) {
            additionalOutputTypeCount++;
            addLootOutput(currentPoint, maxAllowedWidth, maxWidth, additionalOutputs, display.lootOutputId);
        }

        if (additionalOutputTypeCount > 0) {
            // translate up to make them vertically centered;
            // exclude label if not tall (usually only one additional output)
            final int verticalAdjustment = getVerticalAdjustment(centerLeft, currentPoint, additionalOutputTypeCount);

            additionalOutputs.forEach(output -> output.translate(0, verticalAdjustment));
        } else if (display.blockOutput == null) {
            // no outputs
            final var noOutputLabel = new MovableLabel(currentPoint, NO_OUTPUT_LABEL).alignedLeft();
            noOutputLabel.tooltip(Text.literal(fuzzyWrappedSentence(NO_OUTPUT_TOOLTIP.getString(), getTooltipWidth())));
            currentPoint.y -= HALF_LABEL_HEIGHT - 2;
            additionalOutputs.add(noOutputLabel.cast());
            maxWidth.update(noOutputLabel.getBounds().width);
        }

        outputWidgets.addAll(additionalOutputs);

        return new Pair<>(outputWidgets, minX + maxWidth.get());
    }

    private static int getVerticalAdjustment(
        ImmutablePoint centerLeft, Point currentPoint, int additionalOutputTypeCount
    ) {
        int verticalAdjustmentMagnitude = currentPoint.y - centerLeft.y - 7;
        verticalAdjustmentMagnitude = additionalOutputTypeCount == 1 ?
            greaterHalf(verticalAdjustmentMagnitude - LABEL_HEIGHT) + LABEL_HEIGHT :
            greaterHalf(verticalAdjustmentMagnitude);

        return -verticalAdjustmentMagnitude;
    }

    private <T extends WidgetWithBounds & MovableWidget> void addLootOutput(
        Point currentPoint, int maxAllowedWidth, Util.MaxInt maxWidth,
        List<T> additionalOutputs, Identifier lootTableOutputId
    ) {
        if (AnvilCrushingPlugin.RER_LOADED) {
            final var infoLabel = new InfoLabel(currentPoint.clone(), LOOT_OUTPUT_LABEL);
            addLabel(currentPoint, maxWidth, additionalOutputs, infoLabel.mainTooltip(LOOT_OUTPUT_TOOLTIP));

            final var lootDisplayMaxBounds = adjustAndGetLootBounds(currentPoint, maxAllowedWidth);
            final var lootWidget = LootWidget.create(lootTableOutputId, lootDisplayMaxBounds);
            additionalOutputs.add(lootWidget.cast());
            infoLabel.setInfo(lootWidget.getTooltip());

            final var lootBounds = lootWidget.getBounds();
            maxWidth.update(lootBounds.width);
            currentPoint.y += lootBounds.height + V_PADDING;
        } else {
            addLabelWithTooltip(
                currentPoint, maxWidth, additionalOutputs,
                new MovableLabel(currentPoint.clone(), LOOT_OUTPUT_LABEL),
                LOOT_OUTPUT_TOOLTIP
            );

            final var idLabel = makeIdLabel(currentPoint.clone(), lootTableOutputId, maxAllowedWidth);
            idLabel.leftAligned();
            currentPoint.y += LABEL_HEIGHT;
            additionalOutputs.add(idLabel.cast());
            maxWidth.update(idLabel.getBounds().width);
        }
    }

    private <T extends WidgetWithBounds & MovableWidget> void addItemsOutput(
        Point currentPoint, int minX, int maxAllowedCols, Util.MaxInt maxWidth,
        List<T> additionalOutputs, List<ItemStack> itemOutputs
    ) {
        addLabelWithTooltip(
            currentPoint, maxWidth, additionalOutputs,
            new MovableLabel(currentPoint.clone(), ITEM_OUTPUTS_LABEL),
            ITEM_OUTPUTS_TOOLTIP
        );

        currentPoint.y--;

        final int numOutputs = itemOutputs.size();
        final int numColumns = Math.min(numOutputs, maxAllowedCols);
        final int numRows = Util.ceilDiv(numOutputs, maxAllowedCols);

        final var slotBase = new MovableSlotBase(new Rectangle(
            currentPoint.x, currentPoint.y,
            numColumns * SLOT_SIDE_LEN, numRows * SLOT_SIDE_LEN
        ));

        additionalOutputs.add(slotBase.cast());

        currentPoint.translate(1, 1);
        for (int row = 0, i = 0; row < numRows; row++) {
            for (int col = 0; col < numColumns; col++,i++) {
                additionalOutputs.add(
                    new MovableSlotWidget(currentPoint)
                        .entry(EntryStacks.of(itemOutputs.get(i)))
                        .cast()
                );

                if (i == numOutputs) break;
                currentPoint.x += SLOT_SIDE_LEN;
            }

            currentPoint.x = minX;
            currentPoint.y += SLOT_SIDE_LEN;

            if (i == numOutputs) break;
        }

        maxWidth.update(numColumns * SLOT_SIDE_LEN);
        currentPoint.y += V_PADDING;
    }

    private <T extends WidgetWithBounds & MovableWidget> boolean tryAddIngredientLoot(
        AnvilCrushingRecipeDisplay display,
        Point currentPoint, Util.MaxInt maxWidth, int maxAllowedWidth,
        Slot finalInputSlot, List<T> additionalOutputs
    ) {
        if (display.ingredientDropChance <= 0) return false;

        assert (display.finalIngredientLoot != null);

        final var ingredientLootLabel = display.ingredientDropChance == 1 ?
            INGREDIENT_LOOT_LABEL :
            INGREDIENT_LOOT_LABEL.copy().append(Text.literal(" (" + display.ingredientDropChance * 100 + "%)"));

        final var firstLootTableId = display.finalIngredientLoot.size() > 1 ?
            null :
            display.finalIngredientLoot.values().iterator().next();

        if (AnvilCrushingPlugin.RER_LOADED) {
            final var infoLabel = new InfoLabel(currentPoint.clone(), ingredientLootLabel);
            addLabel(currentPoint, maxWidth, additionalOutputs, infoLabel.mainTooltip(INGREDIENT_LOOT_TOOLTIP));

            final var lootDisplayMaxBounds = adjustAndGetLootBounds(currentPoint, maxAllowedWidth);
            final var lootWidget = firstLootTableId == null ?
                new EntrySyncedLootWidget(lootDisplayMaxBounds, finalInputSlot, display.finalIngredientLoot) :
                LootWidget.create(firstLootTableId, lootDisplayMaxBounds);

            additionalOutputs.add(lootWidget.cast());
            infoLabel.setInfo(lootWidget.getTooltip());

            final var lootBounds = lootWidget.getBounds();
            maxWidth.update(lootBounds.width);
            currentPoint.y += lootBounds.height + V_PADDING;
        } else {
            addLabelWithTooltip(
                currentPoint, maxWidth, additionalOutputs,
                new MovableLabel(currentPoint.clone(), ingredientLootLabel),
                INGREDIENT_LOOT_TOOLTIP
            );

            currentPoint.y--;

            final var idsLabel = firstLootTableId == null ?
                new EntrySyncedIdsLabel(
                    currentPoint.clone(), maxAllowedWidth,
                    finalInputSlot, display.finalIngredientLoot
                ).alignedLeft() :
                makeIdLabel(currentPoint, firstLootTableId, maxAllowedWidth);

            additionalOutputs.add(idsLabel.cast());

            final var idsLabelBounds = idsLabel.getBounds();
            maxWidth.update(idsLabelBounds.width);
            currentPoint.y += idsLabelBounds.height;
        }

        return true;
    }

    @NotNull
    private Rectangle adjustAndGetLootBounds(Point currentPoint, int width) {
        return new Rectangle(currentPoint.x, currentPoint.y -= 2, width, LOOT_HEIGHT);
    }

    private <T extends WidgetWithBounds & MovableWidget> void addLabelWithTooltip(
        Point currentPoint, Util.MaxInt maxWidth,
        List<T> additionalOutputs, MovableLabel label, Text tooltip
    ) {
        addLabel(currentPoint, maxWidth, additionalOutputs, label);
        label.tooltip(tooltip);
    }

    private <T extends WidgetWithBounds & MovableWidget> void addLabel(Point currentPoint, Util.MaxInt maxWidth, List<T> additionalOutputs, MovableLabel label) {
        label.leftAligned();
        currentPoint.y += LABEL_HEIGHT;
        additionalOutputs.add(label.cast());
        maxWidth.update(label.getBounds().width);
    }

    @Override
    public CategoryIdentifier<? extends AnvilCrushingRecipeDisplay> getCategoryIdentifier() {
        return ID;
    }

    public interface Measurements {
        int H_PADDING = 6;
        int V_PADDING = 4;

        int SLOT_SIDE_LEN = Widgets.createSlot(ORIGIN).getBounds().width;
        int COLUMN_SPACING = SLOT_SIDE_LEN - 6;

        int LABEL_HEIGHT = Widgets.createLabel(new Point(), Text.literal("")).getBounds().height;
        int HALF_LABEL_HEIGHT = greaterHalf(LABEL_HEIGHT);

        int LOOT_HEIGHT = SLOT_SIDE_LEN + 3;
    }

    public interface Texts {
        Text TITLE = Text.translatable("title" + DOT_NAMESPACE_DOT + "anvil_crushing_recipe");

        Text ANVIL_TOOLTIP = Text.translatable("tooltip" + DOT_NAMESPACE_DOT + "anvil");

        Text FROM_PREFIX = Text.translatable("tooltip" + DOT_NAMESPACE_DOT + "from_prefix");

        Text BLOCK_OUTPUT_LABEL = Text.translatable("label" + DOT_NAMESPACE_DOT + "block_output");
        Text BLOCK_OUTPUT_TOOLTIP = Text.translatable("tooltip" + DOT_NAMESPACE_DOT + "block_output");

        Text INGREDIENT_LOOT_LABEL = Text.translatable("label" + DOT_NAMESPACE_DOT + "ingredient_loot");
        Text INGREDIENT_LOOT_TOOLTIP =
            Text.translatable("tooltip" + DOT_NAMESPACE_DOT + "ingredient_loot");

        Text ITEM_OUTPUTS_LABEL = Text.translatable("label" + DOT_NAMESPACE_DOT + "item_outputs");
        Text ITEM_OUTPUTS_TOOLTIP= Text.translatable("tooltip" + DOT_NAMESPACE_DOT + "item_outputs");

        Text LOOT_OUTPUT_LABEL = Text.translatable("label" + DOT_NAMESPACE_DOT + "loot_output");
        Text LOOT_OUTPUT_TOOLTIP = Text.translatable("tooltip" + DOT_NAMESPACE_DOT + "loot_output");

        Text NO_OUTPUT_LABEL = Text.translatable("label" + DOT_NAMESPACE_DOT + "no_output");
        Text NO_OUTPUT_TOOLTIP = Text.translatable("tooltip" + DOT_NAMESPACE_DOT + "no_output");
    }
}
