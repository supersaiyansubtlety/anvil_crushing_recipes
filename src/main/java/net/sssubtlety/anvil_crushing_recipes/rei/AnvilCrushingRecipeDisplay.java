package net.sssubtlety.anvil_crushing_recipes.rei;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import me.shedaniel.rei.api.common.category.CategoryIdentifier;
import me.shedaniel.rei.api.common.display.basic.BasicDisplay;
import me.shedaniel.rei.api.common.entry.EntryIngredient;
import me.shedaniel.rei.api.common.entry.EntryStack;
import me.shedaniel.rei.api.common.util.EntryIngredients;
import me.shedaniel.rei.api.common.util.EntryStacks;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;
import net.sssubtlety.anvil_crushing_recipes.recipe.AnvilCrushingRecipe;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

import static net.sssubtlety.anvil_crushing_recipes.rei.rer.AbstractLootWidget.lootIdToEntryIngredient;

@Environment(EnvType.CLIENT)
public class AnvilCrushingRecipeDisplay extends BasicDisplay {
    public final Identifier id;
    public final float ingredientDropChance;
    public final @Nullable ImmutableMap<EntryStack<?>, Identifier> finalIngredientLoot;
    public final @Nullable BlockItem blockOutput;
    public final @Nullable List<ItemStack> itemOutputs;
    public final @Nullable Identifier lootOutputId;

    // TODO: implement groups (ex: group all coral->dead_coral)

    public AnvilCrushingRecipeDisplay(AnvilCrushingRecipe.ClientRecipe recipe) {
        super(null, null, Optional.of(recipe.id));
        this.id = recipe.id;
        this.inputs = recipe.ingredients.stream().map(EntryIngredients::ofItemStacks).collect(Collectors.toList());
        this.ingredientDropChance = recipe.ingredientDropChance;
        this.finalIngredientLoot = recipe.finalIngredientLoot == null ? null : recipe.finalIngredientLoot.entrySet().stream().collect(
            ImmutableMap.toImmutableMap(
                entry -> EntryStacks.of(entry.getKey()),
                Map.Entry::getValue,
                (entryA, entryB) -> entryA.equals(Registries.ITEM.getId(Items.AIR)) ? entryB : entryA
            ));

        this.blockOutput = recipe.blockOutput;
        this.itemOutputs = recipe.itemOutputs;
        this.lootOutputId = recipe.lootOutputId;
        this.outputs = getOutputs(recipe, this.finalIngredientLoot == null ? null : this.finalIngredientLoot.values());
    }

    private static List<EntryIngredient> getOutputs(AnvilCrushingRecipe.ClientRecipe recipe, @Nullable ImmutableCollection<Identifier> finalIngredientLootIds) {
        final EntryIngredient.Builder outputsBuilder = EntryIngredient.builder();

        final BlockItem blockOutput = recipe.blockOutput;
        if (blockOutput != null)
            outputsBuilder.add(EntryStacks.of(blockOutput));

        if (AnvilCrushingPlugin.RER_LOADED && finalIngredientLootIds != null)
            finalIngredientLootIds.forEach(id -> lootIdToEntryIngredient(id).forEach(outputsBuilder::addAll));

        final List<ItemStack> itemOutputs = recipe.itemOutputs;
        if (itemOutputs != null) outputsBuilder.addAll(itemOutputs.stream().map(EntryStacks::of).collect(Collectors.toList()));

        if (AnvilCrushingPlugin.RER_LOADED) {
            final Identifier lootOutputId = recipe.lootOutputId;
            if (lootOutputId != null) lootIdToEntryIngredient(lootOutputId).forEach(outputsBuilder::addAll);
        }

        return List.of(outputsBuilder.build());
    }

    @Override
    public CategoryIdentifier<?> getCategoryIdentifier() {
        return AnvilCrushingCategory.ID;
    }
}
