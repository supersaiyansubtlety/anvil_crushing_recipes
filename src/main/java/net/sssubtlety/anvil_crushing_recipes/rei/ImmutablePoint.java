package net.sssubtlety.anvil_crushing_recipes.rei;

import me.shedaniel.math.Point;

public class ImmutablePoint extends Point {

    public ImmutablePoint() { }

    public ImmutablePoint(Point p) {
        super(p);
    }

    @Override
    public void setLocation(double x, double y) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void move(int x, int y) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void translate(int dx, int dy) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
}
