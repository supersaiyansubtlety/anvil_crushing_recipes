package net.sssubtlety.anvil_crushing_recipes.rei;

import me.shedaniel.rei.api.client.plugins.REIClientPlugin;
import me.shedaniel.rei.api.client.registry.category.CategoryRegistry;
import me.shedaniel.rei.api.client.registry.display.DisplayRegistry;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.sssubtlety.anvil_crushing_recipes.Networking.ListenPayload;
import net.sssubtlety.anvil_crushing_recipes.Networking.RecipesPayload;
import net.sssubtlety.anvil_crushing_recipes.recipe.AnvilCrushingRecipe;

import java.util.List;

import static net.sssubtlety.anvil_crushing_recipes.rei.AnvilCrushingCategory.ICON;

@Environment(EnvType.CLIENT)
public class AnvilCrushingPlugin implements REIClientPlugin {
    public static final boolean RER_LOADED;

    private static List<AnvilCrushingRecipe.ClientRecipe> recipes;

    private static void becomeListener(ClientPlayNetworkHandler handler, PacketSender sender, MinecraftClient client) {
        ClientPlayNetworking.send(new ListenPayload());
    }

    private static void receiveRecipes(RecipesPayload payload, ClientPlayNetworking.Context context) {
        recipes = payload.recipes().stream().map(AnvilCrushingRecipe.ClientRecipe::new).toList();
    }

    static {
        RER_LOADED = FabricLoader.getInstance().isModLoaded("roughlyenoughresources");

        ClientPlayConnectionEvents.JOIN.register(AnvilCrushingPlugin::becomeListener);
        ClientPlayNetworking.registerGlobalReceiver(RecipesPayload.ID, AnvilCrushingPlugin::receiveRecipes);
    }

    @Override
    public void registerCategories(CategoryRegistry registry) {
        registry.add(new AnvilCrushingCategory());
        registry.addWorkstations(AnvilCrushingCategory.ID, ICON);
    }

    @Override
    public void registerDisplays(DisplayRegistry registry) {
        recipes.stream().map(AnvilCrushingRecipeDisplay::new).forEach(registry::add);
    }
}
