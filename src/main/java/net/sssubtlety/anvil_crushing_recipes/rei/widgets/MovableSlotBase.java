package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.widgets.Panel;
import me.shedaniel.rei.api.client.gui.widgets.Widgets;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.util.math.MatrixStack;

import java.util.List;

public class MovableSlotBase extends MovableWidget.WithBounds {

    private final Panel delegate;

    public MovableSlotBase(Rectangle rectangle) {
        this.delegate = Widgets.createSlotBase(rectangle);
    }

    @Override
    public Rectangle getBounds() {
        return delegate.getBounds();
    }

    @Override
    public List<? extends Element> children() {
        return delegate.children();
    }

    @Override
    public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
        delegate.render(graphics, mouseX, mouseY, delta);
    }
}
