package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.Renderer;
import me.shedaniel.rei.api.client.gui.widgets.Tooltip;
import me.shedaniel.rei.api.client.gui.widgets.TooltipContext;
import me.shedaniel.rei.api.client.gui.widgets.WidgetWithBounds;
import me.shedaniel.rei.api.client.gui.widgets.Widgets;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.Item;
import net.minecraft.text.Text;

import java.util.List;

public class MovableRendererWithBounds extends MovableWidget.WithBounds {
    private final WidgetWithBounds delegate;

    private Text tooltipText;

    public MovableRendererWithBounds(Rectangle rectangle, Renderer renderer) {
        this.delegate = Widgets.wrapRenderer(rectangle, renderer);
    }

    public void tooltip(Text tooltipText) {
        this.tooltipText = tooltipText;
    }

    public MovableRendererWithBounds withTooltip(Text text) {
        this.tooltip(text);
        return this;
    }

    @Override
    public Rectangle getBounds() {
        return delegate.getBounds();
    }

    @Override
    public List<? extends Element> children() {
        return List.of(delegate);
    }

    @Override
    public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
        delegate.render(graphics, mouseX, mouseY, delta);
        if (containsMouse(mouseX, mouseY)) {
            final var tooltip = getTooltip(TooltipContext.of(new Point(mouseX, mouseY), Item.TooltipContext.EMPTY));

            if (tooltip != null) tooltip.queue();
        }
    }

    @Override
    public Tooltip getTooltip(TooltipContext mouseContext) {
        Tooltip tooltip = super.getTooltip(mouseContext);
        if (tooltipText != null) {
            if (tooltip == null) tooltip = Tooltip.create();
            tooltip.add(tooltipText);
        }
        return tooltip;
    }
}
