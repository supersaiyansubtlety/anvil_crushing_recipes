package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.widgets.Label;
import me.shedaniel.rei.api.client.gui.widgets.Widgets;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.text.StringVisitable;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

public class MovableLabel extends Label implements MovableWidget {
    protected final Label delegate;

    public MovableLabel(Point point, Text text) {
        this(Widgets.createLabel(point, text));
    }

    public void translate(int dx, int dy) {
        delegate.getPoint().translate(dx, dy);
    }

    private MovableLabel(Label delegate) {
        this.delegate = delegate;
    }

//    public MovableLabel tooltip(String... lines) {
//        this.delegate.tooltipLines(lines);
//        return this;
//    }

    public MovableLabel alignedLeft() {
        this.delegate.leftAligned();
        return this;
    }

    @Override public Rectangle getBounds() { return delegate.getBounds(); }
    @Override public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) { delegate.render(graphics, mouseX, mouseY, delta); }
    @Override public List<? extends Element> children() { return delegate.children(); }
    @Override public boolean isClickable() { return delegate.isClickable(); }
    @Override public void setClickable(boolean clickable) { delegate.setClickable(clickable); }
    @Override public @Nullable Consumer<Label> getOnClick() { return delegate.getOnClick(); }
    @Override public void setOnClick(@Nullable Consumer<Label> onClick) { delegate.setOnClick(onClick); }
    @Override public @Nullable BiConsumer<GuiGraphics, Label> getOnRender() { return delegate.getOnRender(); }
    @Override public void setOnRender(@Nullable BiConsumer<GuiGraphics, Label> onRender) { delegate.setOnRender(onRender); }
    @Override public boolean isFocusable() { return delegate.isFocusable(); }
    @Override public void setFocusable(boolean focusable) { delegate.setFocusable(focusable); }
//    @Override @Nullable public String getTooltip() { return delegate.getTooltip(); }

    @Override
    public @Nullable Text[] getTooltipLines() {
        return delegate.getTooltipLines();
    }

    @Override
    public void setTooltipFunction(@Nullable Function<Label, @Nullable Text[]> tooltip) {
        delegate.setTooltipFunction(tooltip);
    }

//    @Override public void setTooltipFunction(@Nullable Function<Label, @Nullable String> tooltip) { delegate.setTooltip(tooltip); }



    @Override public int getHorizontalAlignment() { return delegate.getHorizontalAlignment(); }
    @Override public void setHorizontalAlignment(int horizontalAlignment) { delegate.setHorizontalAlignment(horizontalAlignment); }
    @Override public boolean hasShadow() { return delegate.hasShadow(); }
    @Override public void setShadow(boolean hasShadow) { delegate.setShadow(hasShadow); }
    @Override public int getColor() { return delegate.getColor(); }
    @Override public void setColor(int color) { delegate.setColor(color); }
    @Override public int getHoveredColor() { return delegate.getHoveredColor(); }
    @Override public void setHoveredColor(int hoveredColor) { delegate.setHoveredColor(hoveredColor); }
    @Override public Point getPoint() { return delegate.getPoint(); }
    @Override public void setPoint(Point point) { delegate.setPoint(point); }
    @Override public StringVisitable getMessage() { return delegate.getMessage(); }
    @Override public void setMessage(StringVisitable message) { delegate.setMessage(message); }
    @Override public void setRainbow(boolean rainbow) { delegate.setRainbow(rainbow); }
}
