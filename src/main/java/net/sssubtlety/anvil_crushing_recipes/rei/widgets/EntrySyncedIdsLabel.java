package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import com.google.common.collect.ImmutableMap;
import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.widgets.Slot;
import me.shedaniel.rei.api.common.entry.EntryStack;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.sssubtlety.anvil_crushing_recipes.util.Util;
import net.sssubtlety.anvil_crushing_recipes.rei.ReiUtils;
import net.sssubtlety.anvil_crushing_recipes.util.TextUtil;

import java.util.Map;

public class EntrySyncedIdsLabel extends MovableLabel {
    protected final Slot ingredientSlot;
    protected final ImmutableMap<EntryStack<?>, Text> entry2LootTextMap;
    protected final int width;
    protected Text lootTextCache;

    public EntrySyncedIdsLabel(
        Point point, int maxWidth, Slot ingredientSlot,
        ImmutableMap<EntryStack<?>, Identifier> finalIngredientLoot
    ) {
        super(point, Text.of(""));
        this.ingredientSlot = ingredientSlot;
        ImmutableMap.Builder<EntryStack<?>, Text> entry2LootTextMapBuilder = ImmutableMap.builder();
        Util.MaxInt maxIdTextWidth = new Util.MaxInt(0);
        StringBuilder tooltipBuilder = new StringBuilder();
        int tooltipWidth = ReiUtils.getTooltipWidth();
        for (Map.Entry<EntryStack<?>, Identifier> entry2IdMapping : finalIngredientLoot.entrySet()) {
            Identifier id = entry2IdMapping.getValue();
            final Text idText = TextUtil.truncatedIdText(id, maxWidth);
            entry2LootTextMapBuilder.put(entry2IdMapping.getKey(), idText);
            maxIdTextWidth.update(font.getWidth(idText));
            tooltipBuilder.append(TextUtil.wrappedIdString(id, tooltipWidth)).append(",\n");
        }
        // remove trailing ",\n"
        final int length = tooltipBuilder.length();
        tooltipBuilder.delete(length - 2, length);

        this.entry2LootTextMap = entry2LootTextMapBuilder.build();
        this.width = maxIdTextWidth.get();
        this.delegate.tooltip(Text.literal(tooltipBuilder.toString()));
        this.lootTextCache = entry2LootTextMap.get(ingredientSlot.getCurrentEntry());
        this.setMessage(this.lootTextCache);
    }

    @Override
    public Rectangle getBounds() {
        Point point = getPoint();
        if (getHorizontalAlignment() == LEFT_ALIGNED)
            return new Rectangle(point.x - 1, point.y - 5, width + 2, 14);
        if (getHorizontalAlignment() == RIGHT_ALIGNED)
            return new Rectangle(point.x - width - 1, point.y - 5, width + 2, 14);
        return new Rectangle(point.x - width / 2 - 1, point.y - 5, width + 2, 14);
    }

    @Override
    public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
        Text currentLootText = entry2LootTextMap.get(ingredientSlot.getCurrentEntry());
        if (lootTextCache != currentLootText) {
            lootTextCache = currentLootText;
            this.setMessage(lootTextCache);
        }
        delegate.render(graphics, mouseX, mouseY, delta);
    }
}
