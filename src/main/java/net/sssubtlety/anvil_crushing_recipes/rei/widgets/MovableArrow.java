package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.widgets.Arrow;
import me.shedaniel.rei.api.client.gui.widgets.Tooltip;
import me.shedaniel.rei.api.client.gui.widgets.TooltipContext;
import me.shedaniel.rei.api.client.gui.widgets.Widgets;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.item.Item;
import net.minecraft.text.Text;
import net.sssubtlety.anvil_crushing_recipes.util.Util;

import java.util.List;

public class
MovableArrow extends Arrow implements MovableWidget {
    public static final int ARROW_WIDTH;
    public static final int ARROW_HALF_WIDTH;
    public static final int ARROW_HEIGHT;
    public static final int ARROW_TOP_HALF_HEIGHT;

    static {
        Rectangle templateArrowBounds = Widgets.createArrow(new Point()).getBounds();

        ARROW_WIDTH = templateArrowBounds.width;
        ARROW_HALF_WIDTH = Util.greaterHalf(ARROW_WIDTH);
        ARROW_HEIGHT = templateArrowBounds.height;
        ARROW_TOP_HALF_HEIGHT = Util.greaterHalf(ARROW_HEIGHT);
    }

    private final Arrow delegate;
    private Text tooltipText;

    public MovableArrow(Point currentPoint) {
        this.delegate = Widgets.createArrow(currentPoint);
    }

    public MovableArrow withTooltip(Text text) {
        tooltip(text);
        return this;
    }

    public void tooltip(Text text) {
        this.tooltipText = text;
    }

    @Override
    public Rectangle getBounds() {
        return delegate.getBounds();
    }

    @Override
    public List<? extends Element> children() {
        return delegate.children();
    }

    @Override
    public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
        delegate.render(graphics, mouseX, mouseY, delta);
        if (containsMouse(mouseX, mouseY)) {
            final var tooltip = getTooltip(TooltipContext.of(new Point(mouseX, mouseY), Item.TooltipContext.EMPTY));

            if (tooltip != null) tooltip.queue();
        }
    }

    @Override
    public double getAnimationDuration() {
        return delegate.getAnimationDuration();
    }

    @Override
    public void setAnimationDuration(double animationDurationMS) {
        delegate.setAnimationDuration(animationDurationMS);
    }

    @Override
    public Tooltip getTooltip(TooltipContext mouseContext) {
        Tooltip tooltip = super.getTooltip(mouseContext);
        if (tooltipText != null) {
            if (tooltip == null) tooltip = Tooltip.create(mouseContext.getPoint());
            tooltip.add(tooltipText);
        }
        return tooltip;
    }

    @Override
    public void translate(int dx, int dy) {
        this.getBounds().translate(dx, dy);
    }
}
