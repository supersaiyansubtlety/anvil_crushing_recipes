package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import me.shedaniel.rei.api.client.gui.Renderer;
import me.shedaniel.rei.api.client.gui.widgets.Widget;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.util.math.MatrixStack;

import java.awt.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class RendererColumnWidget extends Widget {
    private final List<Renderer> renderers;
//    private final Rectangle bounds;

    public RendererColumnWidget(List<Renderer> renderers) {
        this.renderers = renderers;

    }

    @Override
    public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
        for (Renderer renderer : renderers) {
//            renderer.render(graphics, mouseX, mouseY, delta);
        }
//        renderer.render(graphics, mouseX, mouseY, delta);
    }

    @Override
    public List<? extends Element> children() {
        final List<Element> children = new LinkedList<>();
        for (Renderer  renderer : renderers) {
            if (renderer instanceof Element listener) {
                children.add(listener);
            }
        }
        return children;
    }
}
