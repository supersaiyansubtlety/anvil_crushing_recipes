package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import me.shedaniel.math.Point;
import me.shedaniel.rei.api.client.gui.widgets.Tooltip;
import me.shedaniel.rei.api.client.gui.widgets.TooltipContext;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.Item;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import static net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipes.NAMESPACE;

public class InfoLabel extends MovableLabel {
    private static final MutableText INFO_WIDGET_TEXT;
    private static final int INFO_PADDING = 3;

    static {
        INFO_WIDGET_TEXT = Text.translatable("label." + NAMESPACE + ".info_widget");
        INFO_WIDGET_TEXT.fillStyle(INFO_WIDGET_TEXT.getStyle().withFormatting(Formatting.DARK_GRAY, Formatting.ITALIC));//, Formatting.BOLD));
    }

    protected final MovableLabel infoWidget;

    public InfoLabel(Point point, Text text) {
        super(point, text);
        final Point infoPoint = point.clone();
        infoPoint.translate(super.getBounds().width + INFO_PADDING, 0);
        this.infoWidget = new MovableLabel(infoPoint, INFO_WIDGET_TEXT);
        delegate.getBounds().width += this.infoWidget.getBounds().width + INFO_PADDING;
    }

    public void setInfo(Text text) {
        infoWidget.tooltip(text);
    }

    @Override
    public void translate(int dx, int dy) {
        super.translate(dx, dy);
        infoWidget.translate(dx, dy);
    }

    @Override
    public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
        super.render(graphics, mouseX, mouseY, delta);
        infoWidget.render(graphics, mouseX, mouseY, delta);
        if (infoWidget.containsMouse(mouseX, mouseY)) {
            final var infoTooltip = infoWidget.getTooltip(TooltipContext.of(
                new Point(mouseX, mouseY),
                Item.TooltipContext.EMPTY
            ));

            if (infoTooltip != null) infoTooltip.queue();
        }
    }

    public InfoLabel mainTooltip(Text... lines) {
        // adds tooltip to main label, not the info widget
        this.delegate.tooltip(lines);
        return this;
    }

    public InfoLabel alignedLeft() {
        this.delegate.leftAligned();
        return this;
    }
}
