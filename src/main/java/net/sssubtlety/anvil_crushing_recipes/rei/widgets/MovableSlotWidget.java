package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.widgets.Slot;
import me.shedaniel.rei.api.client.gui.widgets.Widgets;
import me.shedaniel.rei.api.common.entry.EntryStack;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;

import java.util.Collection;
import java.util.List;

public class MovableSlotWidget extends Slot implements MovableWidget {
    protected final Slot delegate;

    public MovableSlotWidget(Point point) {
        this(Widgets.createSlot(point));
    }

    protected MovableSlotWidget(Slot delegate) {
        this.delegate = delegate;
    }

    @Override
    public void translate(int dx, int dy) {
        getBounds().translate(dx, dy);
    }

    /*Delegations that return `this`*/
    @Override
    public MovableSlotWidget interactable(boolean interactable) {
        delegate.interactable(interactable);
        return this;
    }

    @Override
    public MovableSlotWidget clearEntries() {
        delegate.clearEntries();
        return this;
    }

    @Override
    public MovableSlotWidget entry(EntryStack<?> stack) {
        delegate.entry(stack);
        return this;
    }

    @Override
    public MovableSlotWidget entries(Collection<? extends EntryStack<?>> stacks) {
        delegate.entries(stacks);
        return this;
    }

    /*Direct delegations*/
    @Override public EntryStack<?> getCurrentEntry() { return delegate.getCurrentEntry(); }
    @Override public void setNoticeMark(byte mark) { delegate.setNoticeMark(mark); }
    @Override public byte getNoticeMark() { return delegate.getNoticeMark(); }
    @Override public void setInteractable(boolean interactable) { delegate.setInteractable(interactable); }
    @Override public boolean isInteractable() { return delegate.isInteractable(); }
    @Override public void setInteractableFavorites(boolean interactableFavorites) { delegate.setInteractableFavorites(interactableFavorites); }
    @Override public boolean isInteractableFavorites() { return delegate.isInteractableFavorites(); }
    @Override public void setHighlightEnabled(boolean highlights) { delegate.setHighlightEnabled(highlights); }
    @Override public boolean isHighlightEnabled() { return delegate.isHighlightEnabled(); }
    @Override public void setTooltipsEnabled(boolean tooltipsEnabled) { delegate.setTooltipsEnabled(tooltipsEnabled); }
    @Override public boolean isTooltipsEnabled() { return delegate.isTooltipsEnabled(); }
    @Override public void setBackgroundEnabled(boolean backgroundEnabled) { delegate.setBackgroundEnabled(backgroundEnabled); }
    @Override public boolean isBackgroundEnabled() { return delegate.isBackgroundEnabled(); }
    @Override public List<EntryStack<?>> getEntries() { return delegate.getEntries(); }
    @Override public Rectangle getInnerBounds() { return delegate.getInnerBounds(); }
    @Override public Rectangle getBounds() { return delegate.getBounds(); }
    @Override public List<? extends Element> children() { return delegate.children(); }
    @Override public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) { delegate.render(graphics, mouseX, mouseY, delta); }
}
