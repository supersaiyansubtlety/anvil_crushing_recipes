package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import me.shedaniel.math.Rectangle;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.ParentElement;
import net.minecraft.client.util.math.MatrixStack;

import java.util.List;

public class WidgetContainer extends MovableWidget.WithBounds implements ParentElement {
    protected final List<? extends MovableWidget.WithBounds> children;
    protected final Rectangle bounds;

    public WidgetContainer(List<? extends MovableWidget.WithBounds> widgets, Rectangle bounds) {
        this.children = widgets;
        this.bounds = bounds;
    }

    @Override
    public Rectangle getBounds() {
        return bounds;
    }

    @Override
    public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
        children.forEach(widget -> widget.render(graphics, mouseX, mouseY, delta));
    }

    @Override
    public List<? extends Element> children() {
        return children;
    }

    public void translate(int dx, int dy) {
        bounds.translate(dx, dy);
        children.forEach(child -> child.translate(dx, dy));
    }

//    @Override
//    public boolean mouseScrolled(double mouseX, double mouseY, double amount) {
//        boolean result = false;
//        for (WidgetWithBounds child : children)
//            result |= child.mouseScrolled(mouseX, mouseY, amount);
//
//        return result;
//    }
}
