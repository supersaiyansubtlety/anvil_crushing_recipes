package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import me.shedaniel.rei.api.client.gui.widgets.WidgetWithBounds;

public interface MovableWidget {
    void translate(int dx, int dy);

    default <T extends WidgetWithBounds & MovableWidget> T cast() {
        return (T) this;
    }

    abstract class WithBounds extends WidgetWithBounds implements MovableWidget {
        @Override
        public void translate(int dx, int dy) {
            getBounds().translate(dx, dy);
        }
    }
}
