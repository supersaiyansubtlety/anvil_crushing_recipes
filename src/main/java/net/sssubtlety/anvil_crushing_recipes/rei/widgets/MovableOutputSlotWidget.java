package net.sssubtlety.anvil_crushing_recipes.rei.widgets;

import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.widgets.Panel;
import me.shedaniel.rei.api.client.gui.widgets.Widgets;
import me.shedaniel.rei.api.common.entry.EntryStack;
import net.minecraft.client.gui.GuiGraphics;

import java.util.Collection;

import static net.sssubtlety.anvil_crushing_recipes.util.Util.greaterHalf;
import static net.sssubtlety.anvil_crushing_recipes.rei.AnvilCrushingCategory.Measurements.SLOT_SIDE_LEN;

public class MovableOutputSlotWidget extends MovableSlotWidget {
    protected final Panel background;
    public MovableOutputSlotWidget(Point point) {
        super(point);
        this.delegate.setBackgroundEnabled(false);
        this.background = (Panel) Widgets.createResultSlotBackground(point);
        final int slotAdjustment = greaterHalf(this.background.getBounds().width - SLOT_SIDE_LEN) + 1;
//        delegate.getBounds().translate(slotAdjustment, 0);
    }

    @Override
    public void translate(int dx, int dy) {
//        background.getBounds().translate(dx, dy);
//        delegate.getBounds().translate(dx, dy);
        this.getBounds().translate(dx, dy);
        super.getBounds().translate(dx, dy);
    }

    @Override
    public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
        background.render(graphics, mouseX, mouseY, delta);
        super.render(graphics, mouseX, mouseY, delta);
    }

    @Override
    public Rectangle getBounds() {
        return background.getBounds();
    }

    /*Delegations that return `this`*/
    @Override
    public MovableOutputSlotWidget interactable(boolean interactable) {
        delegate.interactable(interactable);
        return this;
    }

    @Override
    public MovableOutputSlotWidget clearEntries() {
        delegate.clearEntries();
        return this;
    }

    @Override
    public MovableOutputSlotWidget entry(EntryStack<?> stack) {
        delegate.entry(stack);
        return this;
    }

    @Override
    public MovableOutputSlotWidget entries(Collection<? extends EntryStack<?>> stacks) {
        delegate.entries(stacks);
        return this;
    }
}
