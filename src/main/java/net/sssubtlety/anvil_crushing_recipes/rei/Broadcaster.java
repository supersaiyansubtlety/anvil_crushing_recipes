package net.sssubtlety.anvil_crushing_recipes.rei;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface Broadcaster<T> {
    T register(@NotNull Listener<T> listener);

    @NotNull Iterable<Listener<T>> getListeners();

    default void broadcast(T t) {
        getListeners().forEach(listener -> listener.listen(t));
    }

    @FunctionalInterface
    interface Listener<T> {
        void listen(@Nullable T t);
    }
}
