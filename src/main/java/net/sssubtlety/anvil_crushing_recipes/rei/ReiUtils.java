package net.sssubtlety.anvil_crushing_recipes.rei;

import me.shedaniel.math.Point;
import net.minecraft.client.MinecraftClient;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.sssubtlety.anvil_crushing_recipes.util.TextUtil;
import net.sssubtlety.anvil_crushing_recipes.rei.widgets.MovableLabel;

public interface ReiUtils {
    static int getTooltipWidth() {
        return MinecraftClient.getInstance().getWindow().getScaledWidth() / 3;
    }

    static MovableLabel makeIdLabel(Point point, Identifier id, int maxWidth) {
        final MovableLabel idLabel = new MovableLabel(point.clone(), TextUtil.truncatedIdText(id, maxWidth));
        idLabel.tooltip(Text.literal(tooltipIdString(id))).leftAligned();
        return idLabel;
    }

    static String tooltipIdString(Identifier id) {
        return TextUtil.wrappedIdString(id, getTooltipWidth());
    }
}
