package net.sssubtlety.anvil_crushing_recipes;

import net.fabricmc.fabric.api.networking.v1.PayloadTypeRegistry;
import net.fabricmc.fabric.api.networking.v1.ServerPlayConnectionEvents;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.packet.payload.CustomPayload;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.sssubtlety.anvil_crushing_recipes.recipe.AnvilCrushingRecipe;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipes.NAMESPACE;

public abstract class Networking {
    private static final Set<ServerPlayerEntity> listeners = new LinkedHashSet<>();

    public static void init() {
        ServerPlayConnectionEvents.DISCONNECT.register(Networking::removeListener);
        ServerPlayNetworking.registerGlobalReceiver(ListenPayload.ID, Networking::addListener);
    }

    private static void removeListener(ServerPlayNetworkHandler handler, MinecraftServer server) {
        listeners.remove(handler.player);
    }

    private static void addListener(ListenPayload payload, ServerPlayNetworking.Context context) {
        listeners.add(context.player());
        notify(RecipesPayload.create(), context.player());
    }

    public static void notifyListeners() {
        if (Networking.listeners.isEmpty()) return;

        final var payload = RecipesPayload.create();
        Networking.listeners.forEach(player -> notify(payload, player));
    }

    private static void notify(RecipesPayload payload, ServerPlayerEntity listener) {
        ServerPlayNetworking.send(listener, payload);
    }

    public record RecipesPayload(List<AnvilCrushingRecipe.Serialized> recipes) implements CustomPayload {
        public static final CustomPayload.Id<RecipesPayload> ID =
            new CustomPayload.Id<>(Identifier.of(NAMESPACE, "recipes"));

        public static final PacketCodec<RegistryByteBuf, RecipesPayload> CODEC =
            AnvilCrushingRecipe.Serialized.LIST_CODEC.map(RecipesPayload::new, RecipesPayload::recipes);


        public static RecipesPayload create() {
            return new RecipesPayload(AnvilCrushingRecipe.serializeRecipes());
        }

        static {
            PayloadTypeRegistry.playS2C().register(RecipesPayload.ID, RecipesPayload.CODEC);
        }

        @Override
        public Id<RecipesPayload> getId() {
            return ID;
        }
    }

    public record ListenPayload() implements CustomPayload {
        public static final CustomPayload.Id<ListenPayload> ID =
            new CustomPayload.Id<>(Identifier.of(NAMESPACE, "listen"));

        public static final PacketCodec<PacketByteBuf, ListenPayload> CODEC =
            CustomPayload.create(ListenPayload::write, ListenPayload::new);

        static {
            PayloadTypeRegistry.playC2S().register(ListenPayload.ID, ListenPayload.CODEC);
        }

        private void write(PacketByteBuf buf) { }

        private ListenPayload(PacketByteBuf buf) {
            this();
        }

        @Override
        public Id<ListenPayload> getId() {
            return ID;
        }
    }
}
