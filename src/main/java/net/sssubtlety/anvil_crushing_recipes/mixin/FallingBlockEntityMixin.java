package net.sssubtlety.anvil_crushing_recipes.mixin;

import com.mojang.datafixers.util.Either;
import net.minecraft.block.AnvilBlock;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.FallingBlockEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;
import net.minecraft.nbt.NbtString;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.function.BooleanBiFunction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.WorldEvents;
import net.sssubtlety.anvil_crushing_recipes.recipe.AnvilCrushingRecipe;
import net.sssubtlety.anvil_crushing_recipes.recipe.AnvilCrushingRecipe.Match;
import net.sssubtlety.anvil_crushing_recipes.util.Util;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import static net.sssubtlety.anvil_crushing_recipes.util.Util.*;

@Mixin(FallingBlockEntity.class)
abstract class FallingBlockEntityMixin extends Entity {
    // TODO deprecate these awful keys
    @Unique private static final String ID_STRINGS_KEY = "id_strings";
    @Unique private static final String IS_BLOCK_BYTES_KEY = "is_block_bytes";

    @Unique
    private static void writeCrushedList(NbtCompound nbt, List<Either<BlockState, EntityType<?>>> collidedList) {
        final int numCrushed = collidedList.size();
        final byte[] isBlockBytes = new byte[numCrushed];
        final var idStrings = new NbtList();

        for (int i = 0; i < numCrushed; i++) {
            final var blockStateOrEntityType = collidedList.get(i);
            if (blockStateOrEntityType.left().isPresent()) {
                isBlockBytes[i] = 1;
                idStrings.add(NbtString.of(Registries.BLOCK.getId(blockStateOrEntityType.orThrow().getBlock()).toString()));
            } else {
                isBlockBytes[i] = 0;
                idStrings.add(NbtString.of(Registries.ENTITY_TYPE.getId(blockStateOrEntityType.swap().orThrow()).toString()));
            }
        }

        nbt.putByteArray(IS_BLOCK_BYTES_KEY, isBlockBytes);
        nbt.put(ID_STRINGS_KEY, idStrings);
    }

    @Unique
    private static List<Either<BlockState, EntityType<?>>> readCrushedList(NbtCompound nbt) {
        final byte[] isBlockBytes = nbt.getByteArray(IS_BLOCK_BYTES_KEY);
        final var idStrings = (NbtList) nbt.get(ID_STRINGS_KEY);
        final List<Either<BlockState, EntityType<?>>> crushed = new ArrayList<>();

        if (idStrings == null) return crushed;

        final int numCrushed = idStrings.size();
        for (int i = 0; i < numCrushed; i++) {
            final var id = Identifier.parse(idStrings.get(i).asString());
            if (isBlockBytes[i] > 0)
                // is block
                crushed.add(Either.left(Registries.BLOCK.get(id).getDefaultState()));
            else // is entity
                crushed.add(Either.right(Registries.ENTITY_TYPE.get(id)));
        }

        return crushed;
    }

    @Shadow public abstract BlockState getBlockState();

    @Unique private List<Either<BlockState, EntityType<?>>> crushed;

    private FallingBlockEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null);
        throw new IllegalStateException("FallingBlockEntityMixin's dummy constructor called.");
    }

    @Inject(method = "<init>(Lnet/minecraft/entity/EntityType;Lnet/minecraft/world/World;)V", at = @At("TAIL"))
    private void postConstruction(CallbackInfo ci) {
        crushed = new ArrayList<>();
    }

    @Inject(method = "tick", cancellable = true, at = @At(
        value = "INVOKE", shift = At.Shift.AFTER,
        target = "Lnet/minecraft/entity/FallingBlockEntity;move(Lnet/minecraft/entity/MovementType;Lnet/minecraft/util/math/Vec3d;)V"
    ))
    private void postMovementTryOnLandingCollision(CallbackInfo ci) {
        if (
            this.getWorld().isClient ||
            this.notAnvil() ||
            !this.isOnGround() ||
            this.isInsideBlock()
        ) return;

        final var here = this.getBlockPos();
        final var stateHere = this.getWorld().getBlockState(here);
        final boolean emptyHere = Util.isEmptyState(stateHere);
        if (!onLandingCollision(here, stateHere, emptyHere)) {
            this.setOnGround(false);
            ci.cancel();
        }
    }

    @ModifyArg(method = "handleFallDamage", at = @At(
        value = "INVOKE",
        target = "Ljava/util/List;forEach(Ljava/util/function/Consumer;)V"
    ))
    private Consumer<Entity> addCollisionListener(Consumer<Entity> original) {
        if (this.notAnvil()) return original;

        return collidedEntity -> {
            onEntityCollision(collidedEntity);
            original.accept(collidedEntity);
        };
    }

    @Inject(method = "writeCustomDataToNbt", at = @At("HEAD"))
    private void writeCrushedList(NbtCompound nbt, CallbackInfo ci) {
        if (notAnvil()) return;
        writeCrushedList(nbt, crushed);
    }

    @Inject(method = "readCustomDataFromNbt", at = @At("HEAD"))
    private void readCrushedList(NbtCompound nbt, CallbackInfo ci) {
        if (notAnvil()) return;
        crushed = readCrushedList(nbt);
    }

    // return true iff should land as normal
    @Unique
    private boolean onLandingCollision(BlockPos here, BlockState stateHere, boolean emptyHere) {
        BlockState collidedState;
        BlockPos collidedPos;
        // landing
        if (emptyHere) {
            // colliding with space below
            collidedPos = here.down();
            collidedState = this.getWorld().getBlockState(collidedPos);
        } else {
            // colliding with this space; if placed, the anvil must be in the space above
            collidedState = stateHere;
            collidedPos = here;
            here = here.up();
        }

        return onBlockCollision(collidedState, collidedPos, here);
    }

    @Unique
    private void onEntityCollision(Entity collidedEntity) {
        if (this.getWorld().isClient) return;
        if (!collidedEntity.isAlive() || collidedEntity.isInvulnerableTo(this.getWorld().getDamageSources().fallingAnvil(this))) return;

        final var here = this.getBlockPos();
        final var recipeMatch = this.getRecipeFromEntity(collidedEntity);

        if (recipeMatch.getLeft() == Match.FULL) {
            final var recipe = recipeMatch.getRight();
            final BlockPos outputPos;
            if (recipe.blockOutput != null) {
                this.centerInBlock(here);
                outputPos = here.down();
            } else {
                outputPos = collidedEntity.getBlockPos();
            }

            recipe.output(outputPos, collidedEntity.getWorld(), null);
        }
    }

    // returns true iff landing
    @Unique
    private boolean onBlockCollision(BlockState collidedState, BlockPos collidedPos, BlockPos here) {
        final var recipeMatch = getRecipeMatchFromBlock(collidedState, collidedPos);
        final var match = recipeMatch.getLeft();

        if (match == Match.NONE) return true;

        final boolean landing;
        if (match == Match.FULL) {
            final var recipe = recipeMatch.getRight();
            if (recipe.blockOutput != null) {
                landing = true;

                // teleport to block center so there's no overlap with the block space collided with
                centerInBlock(here);
            } else landing = false;

            recipe.output(collidedPos, this.getWorld(), collidedState);
        } else landing = false;

        if (!landing) {
            this.setOnGround(false);
            this.getWorld().syncWorldEvent(WorldEvents.ANVIL_LANDS, this.getBlockPos(), 0);
            this.scheduleVelocityUpdate();
            this.setVelocity(Vec3d.ZERO);
        } else {
            // This makes the vanilla code place the anvil if there's a block
            //   below even if that block isn't one it would usually land on
            this.setOnGround(true);
        }

        return landing;
    }

    @Unique
    private Pair<Match, AnvilCrushingRecipe> getRecipeMatchFromBlock(
        BlockState collidedState, BlockPos pos
    ) {
        crushed.add(Either.left(collidedState));

        final var recipeMatch = AnvilCrushingRecipe.getRecipeMatch(crushed);
        final var match = recipeMatch.getLeft();

        if (match != Match.NONE) recipeMatch.getRight().crushBlock(pos, this.getWorld());

        if (match != Match.PARTIAL) crushed.clear();

        return recipeMatch;
    }

    @Unique
    private Pair<Match, AnvilCrushingRecipe> getRecipeFromEntity(Entity entity) {
        crushed.add(Either.right(entity.getType()));

        final var recipeMatch = AnvilCrushingRecipe.getRecipeMatch(crushed);
        final var match = recipeMatch.getLeft();

        if (match != Match.NONE) recipeMatch.getRight().crushEntity(this.getWorld(), entity, this);

        if (match != Match.PARTIAL) crushed.clear();

        return recipeMatch;
    }

    @Unique
    private boolean isInsideBlock() {
        if (this.noClip) return false;
        final var box = calculateBoundingBox();
        return BlockPos.stream(box).anyMatch(blockPos -> {
            final var state = this.getWorld().getBlockState(blockPos);
            return !isEmptyState(state) &&
                state.isSolidBlock(this.getWorld(), blockPos) &&
                VoxelShapes.matchesAnywhere(
                    state.getCollisionShape(this.getWorld(), blockPos)
                        .offset(blockPos.getX(), blockPos.getY(), blockPos.getZ()),
                    VoxelShapes.cuboid(box),
                    BooleanBiFunction.AND
                );
        });
    }

    @Unique
    private void centerInBlock(BlockPos pos) {
        final Vec3d centerHere = Vec3d.ofCenter(pos);
        this.refreshPositionAndAngles(centerHere.x, centerHere.y, centerHere.z, this.getYaw(), this.getPitch());
    }

    @Unique
    private boolean notAnvil() {
        return !(this.getBlockState().getBlock() instanceof AnvilBlock);
    }
}
