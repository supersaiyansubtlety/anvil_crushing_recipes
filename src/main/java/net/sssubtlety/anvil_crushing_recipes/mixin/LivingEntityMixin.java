package net.sssubtlety.anvil_crushing_recipes.mixin;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.server.world.ServerWorld;
import net.sssubtlety.anvil_crushing_recipes.mixin_helpers.LivingEntityMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(LivingEntity.class)
abstract class LivingEntityMixin extends Entity implements LivingEntityMixinAccessor {
    @Unique private ThreadLocal<Boolean> drop;
    @Unique private static final float INSTANT_KILL_DAMAGE = 3.4028235E38F;

    @Shadow public abstract boolean damage(DamageSource source, float amount);

    private LivingEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null);
        throw new IllegalStateException("LivingEntityMixin's dummy constructor called. ");
    }

    @Inject(method = "<init>", at = @At("TAIL"))
    private void postConstruction(CallbackInfo ci) {
        this.drop = ThreadLocal.withInitial(() -> true);
    }

    @WrapOperation(method = "onDeath", allow = 1, at = @At(
        value = "INVOKE",
        target = "Lnet/minecraft/entity/LivingEntity;drop(Lnet/minecraft/server/world/ServerWorld;" +
            "Lnet/minecraft/entity/damage/DamageSource;)V"
    ))
    private void conditionallyDrop(
        LivingEntity instance, ServerWorld world, DamageSource source, Operation<Void> original
    ) {
        if (this.drop.get()) original.call(instance, world, source);
        this.drop.set(true);
    }

    @Override
    public void anvil_crushing_recipes$kill(DamageSource source, boolean drop) {
        this.drop.set(drop);
        this.damage(source, INSTANT_KILL_DAMAGE);
    }
}
