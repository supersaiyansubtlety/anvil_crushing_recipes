package net.sssubtlety.anvil_crushing_recipes;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.CommonLifecycleEvents;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.resource.ResourceType;
import net.sssubtlety.anvil_crushing_recipes.recipe.AnvilCrushingRecipe;
import net.sssubtlety.anvil_crushing_recipes.recipe.AnvilCrushingRecipeManager;
import net.sssubtlety.anvil_crushing_recipes.recipe.ingredient.BlockIngredient;
import net.sssubtlety.anvil_crushing_recipes.recipe.ingredient.EntityTypeIngredient;
import net.sssubtlety.anvil_crushing_recipes.util.Util;

import static net.fabricmc.fabric.api.resource.ResourcePackActivationType.DEFAULT_ENABLED;
import static net.fabricmc.fabric.api.resource.ResourcePackActivationType.NORMAL;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        // reference these so classes loaded
        // 	cannot reference static method of super class
        BlockIngredient.EMPTY.init();
        EntityTypeIngredient.EMPTY.init();

        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(AnvilCrushingRecipe.MANAGER);
        CommonLifecycleEvents.TAGS_LOADED.register((registries, client) -> AnvilCrushingRecipeManager.postLoad());

        FabricLoader.getInstance().getModContainer(AnvilCrushingRecipes.NAMESPACE).ifPresent(modContainer -> {

            Util.registerResourcePacks(modContainer, DEFAULT_ENABLED,
                "block_breaking",
                "block_degradation",
                "stone_to_cobble",
                "ice_compression",
                "wither_skele_on_basalt_to_blackstone",
                "crush_ores"
            );

            Util.registerResourcePacks(modContainer, NORMAL, "compress_stone_to_deepslate");
        });

        Networking.init();
    }
}
