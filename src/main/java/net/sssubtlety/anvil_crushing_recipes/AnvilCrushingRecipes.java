package net.sssubtlety.anvil_crushing_recipes;

import net.minecraft.util.Identifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AnvilCrushingRecipes {
	public static final String NAMESPACE = "anvil_crushing_recipes";
    public static final Identifier ID = Identifier.of(NAMESPACE, NAMESPACE);
	public static final Logger LOGGER = LogManager.getLogger();
}
